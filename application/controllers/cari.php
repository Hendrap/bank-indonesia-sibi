<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cari extends CI_Controller {
	function __construct(){
        parent::__construct();
    }
        public function index()
	{
            $isi['content']     = '';
            $isi['tabel']       = 'Cari';
            $isi['sub']         = 'Data';
            $this->load->view('cari/index',$isi);
	}
}