<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tahun_buku extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->model_security->getsecurity();   //untuk sekuriti login
        $this->model_security->justadmin();   //hanya admin yang bisa masuk
        $this->load->model('model_tahun_buku');
    }
        public function index()
	{
            $isi['content']     = 'tahun_buku/view_tahun_buku';
            $isi['tabel']       = 'Tahun_buku';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";
            $isi['data']        = $this->model_tahun_buku->view();
            $this->load->view('view_data',$isi);
	}
        public function tambah()
        {
            $isi['content']     = 'tahun_buku/form_tahun_buku';
            $isi['tabel']       = 'Tahun buku';
            $isi['sub']         = 'Data';

            $isi['kode_tahun'] = "";
            $isi['tahun_buku'] = "";
            $isi['tahun_dokumen'] = "";
            $this->load->view('form',$isi);
    
        }
        public function checkId(){
            $id = $this->input->post('id');
            $this->db->where('kode_tahun',$id);
            $query = $this->db->get('tahun_buku');
            
            if($query->num_rows()>0){
                $data = array(
                    'username' => 0
                        );
            }
            else {
                $data = array(
                    'username' => 1
                        );
            }
            echo json_encode($data);
        }
        public function edit()
        {
            $isi['content']     = 'tahun_buku/edit_tahun_buku';
            $isi['tabel']       = 'Tahun buku';
            $isi['sub']         = 'Data';
            
            $key = $this->uri->segment(3);
            $this->db->where('kode_tahun',$key);
            $query = $this->db->get('tahun_buku');
            if($query->num_rows()>0)
            {
                foreach ($query->result() as $row)
                {
                    $isi['kode_tahun'] = $row->kode_tahun;
                    $isi['tahun_buku'] = $row->tahun_buku;
                    $isi['tahun_dokumen'] = $row->tahun_dokumen;
                }
            }
            else
            {
                    redirect('home/Error404');
            }
            $this->load->view('form',$isi);
    
        }
        
        public function simpan()
        {
            $key = $this->input->post('kode_tahun');
            $data['kode_tahun']   = $this->input->post('kode_tahun');
            $data['tahun_buku']        = $this->input->post('tahun_buku');
            $data['tahun_dokumen']   = $this->input->post('tahun_dokumen');;
        
            $query = $this->model_tahun_buku->getdata($key);
            if($query->num_rows()>0)
            {
                $this->model_tahun_buku->getupdate($key,$data);
                $this->session->set_flashdata('info','data suskses di update');
            }
            else
            {
                $this->model_tahun_buku->getinsert($data);
                $this->session->set_flashdata('info','data suskses di simpan');
            }
            redirect('tahun_buku');
            
        }
        public function delete()
        {
            $key = $this->uri->segment(3);
            //cek apakah constraint
            $this->db->where('tahun_buku',$key);
            $q2 = $this->db->get('dokumen');
            if($q2->num_rows()>0)
            {
                $this->session->set_flashdata('info','(Constraint) Gagal ! data masih digunakan oleh tabel lain.');
                redirect('tahun_buku');
            }
            //delete data
            $this->db->where('kode_tahun',$key);
            $query = $this->db->get('tahun_buku');
            
            if($query->num_rows()>0)
            {
                $this->model_tahun_buku->getdelete($key);
            }
            redirect('tahun_buku');
        }

}