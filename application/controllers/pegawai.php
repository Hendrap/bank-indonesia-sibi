<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pegawai extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->model_security->getsecurity();   //untuk sekuriti login
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('model_pegawai');
        $this->load->model('model_divisi');
    }
        public function index()
    {
            $this->model_security->justAdmin();   //hanya admin yang bisa masuk
            $isi['content']     = 'pegawai/view_pegawai';
            $isi['tabel']       = 'Pegawai';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";
            $isi['data']        = $this->model_pegawai->view();
            //notif
            $isi['notif'] = $this->model_disposisi->getMyTask();

            $this->load->view('view_data',$isi);
    }

        public function myprofile()
    {

            $nip = $this->session->userdata('nip');
            $divisi = $this->session->userdata('kode_divisi');

            $this->load->model('model_disposisi');
            $isi['content']     = 'pegawai/view_profile';
            $isi['tabel']       = 'Pegawai';
            $isi['sub']         = '';
            $isi['kode']        = "";
            //pakai array buat nampung
            $x['data']        = $this->model_pegawai->viewMy($nip);
            foreach ($x['data']->result() as $row) {
                $isi['nip'] = $row->nip;
                $isi['username'] = $row->username;
                $isi['nama_pegawai'] = $row->nama_pegawai;
                $isi['kode_divisi'] = $row->kode_divisi;
                $isi['jabatan'] = $row->jabatan;
                $isi['foto'] = $row->foto;
            }
            //lihat teman
            $isi['friend']    = $this->model_pegawai->viewFriend($divisi,$nip);
            //lihat tugas
            $isi['disposisi'] = $this->model_disposisi->getmydisposisi();
            $isi['activity'] = $this->model_disposisi->getmyact();
            
            $this->load->view('view_home',$isi);
    }
    public function editmyprofile()
        {
            $isi['content']     = 'pegawai/edit_myprofile';
            $isi['tabel']       = 'Pegawai';
            $isi['sub']         = 'Form';
            
            $key = $this->session->userdata('nip');
            $this->db->where('nip',$key);
            $query = $this->db->get('pegawai');
            if($query->num_rows()>0)
            {
                foreach ($query->result() as $row)
                {
                    $isi['nip'] = $row->nip;
                    $isi['username'] = $row->username;
                    $isi['nama_pegawai'] = $row->nama_pegawai;
                    $isi['password'] = $row->password;
                    $isi['jabatan'] = $row->jabatan;
                    $isi['kode_divisi'] = $this->model_divisi->viewAktif();
                    $isi['old_kode_divisi'] = $row->kode_divisi;
                    $isi['temp_foto'] = $row->foto;
                    $isi['aktif'] = $row->aktif;
                }
            }
            else
            {
                    redirect('home/Error404');
            }
            $this->load->view('form',$isi);
    }
    
    public function tambah()
    {

            $this->model_security->justAdmin();   //hanya admin yang bisa masuk
            $isi['content']     = 'pegawai/form_pegawai';
            $isi['tabel']       = 'Pegawai';
            $isi['sub']         = 'Data';

            $isi['nip'] = "";
            $isi['username'] = "";
            $isi['nama_pegawai'] = "";
            $isi['password'] = "";
            $isi['jabatan'] = "pegawai";
            $isi['kode_divisi'] = $this->model_divisi->viewAktif();
            $isi['old_kode_divisi'] ="";
            $isi['temp_foto'] = "";
            $isi['aktif'] = "";
            $this->load->view('form',$isi);
    }
    function uploadImage()
    {

       $config['upload_path']   =   "database/uploads/";
       $config['allowed_types'] =   "gif|jpg|jpeg|png"; 
       $config['max_size']      =   "5000";
       $config['max_width']     =   "1907";
       $config['max_height']    =   "1280";
       $this->load->library('upload',$config);
       if(!$this->upload->do_upload())
       {
            $foto=$this->input->post('old_foto');
           $this->simpan($foto);
       }
       else
       {
           $foto=$this->upload->data();
           $this->createThumbnail($foto['file_name']);
           //$data['uploadInfo'] = $finfo;
           //$data['thumbnail_name'] =  'thumb_'.$finfo['raw_name'].$finfo['file_ext']; 
           $this->simpan($foto['file_name']);
       }
    }

//Create Thumbnail function

    function createThumbnail($filename)
    {
            $config['image_library']    = "gd2";      
            $config['source_image']     = "database/uploads/" .$filename;      
            $config['create_thumb']     = TRUE;      
            $config['maintain_ratio']   = TRUE;      
            $config['width'] = "80";      
            $config['height'] = "80";
            $this->load->library('image_lib',$config);
            if(!$this->image_lib->resize())
            {
                echo $this->image_lib->display_errors();
            }      
    }
    
    public function simpan($foto)
    {
            $key = $this->input->post('nip');
            $data['nip']   = $this->input->post('nip');
            $data['username']   = $this->input->post('username');
            $data['nama_pegawai'] = $this->input->post('nama_pegawai');
            $data['username']   = $this->input->post('username');
            $data['password']   = $this->input->post('password');
            $data['jabatan']   = $this->input->post('jabatan');
            $data['kode_divisi']   = $this->input->post('kode_divisi');
            $data['foto']   = $foto;
            $data['thumb'] = $this->model_pegawai->setthumbname($foto);
            $data['aktif']   = $this->input->post('aktif');
        
            $query = $this->model_pegawai->getdata($key);
            if($query->num_rows()>0)
            {
                $this->model_pegawai->getupdate($key,$data);
                $this->session->set_flashdata('info','data suskses di update');
            }
            else
            {
                $this->model_pegawai->getinsert($data);
                $this->session->set_flashdata('info','data suskses di simpan');
            }

            $level = $this->session->userdata('jabatan');
            if($level=='administrator'){
                redirect('pegawai');       
            }
            else{
                redirect('pegawai/myprofile');
            }
    }
    public function edit()
    {
        $this->model_security->justAdmin();   //hanya admin yang bisa masuk
            $isi['content']     = 'pegawai/edit_pegawai';
            $isi['tabel']       = 'Pegawai';
            $isi['sub']         = 'Data';
            
            $key = $this->uri->segment(3);
            $this->db->where('nip',$key);
            $query = $this->db->get('pegawai');
            if($query->num_rows()>0)
            {
                foreach ($query->result() as $row)
                {
                    $isi['nip'] = $row->nip;
                    $isi['username'] = $row->username;
                    $isi['nama_pegawai'] = $row->nama_pegawai;
                    $isi['password'] = $row->password;
                    $isi['jabatan'] = $row->jabatan;
                    $isi['kode_divisi'] = $this->model_divisi->viewAktif();
                    $isi['old_kode_divisi'] = $row->kode_divisi;
                    $isi['temp_foto'] = $row->foto;
                    $isi['aktif'] = $row->aktif;
                }
            }
            else
            {
                    $isi['nip'] = "";
                    $isi['username'] = "";
                    $isi['nama_pegawai'] = "";
                    $isi['password'] = "";
                    $isi['jabatan'] = "";
                    $isi['kode_divisi'] = "";
                    $isi['foto'] = "";
                    $isi['aktif'] = "";
            }
            $this->load->view('form',$isi);
    
        }

        public function state()
        {
            $this->model_security->justAdmin();   //hanya admin yang bisa masuk
            $key = $this->uri->segment(3);
            $stat = $this->uri->segment(4);
            $this->db->where('nip',$key);
            $query = $this->db->get('pegawai');
            
            if($query->num_rows()>0)
            {
                $this->model_pegawai->changeStatus($key,$stat);
            }
            redirect('pegawai');
        }

        public function checkId(){
            $id = $this->input->post('username');
            $this->db->where('username',$id);
            $query = $this->db->get('pegawai');
            
            if($query->num_rows()>0){
                $data = array(
                    'username' => 0
                        );
            }
            else {
                $data = array(
                    'username' => 1
                        );
            }
            echo json_encode($data);
        }

        public function delete()
        {
            $this->model_security->justAdmin();   //hanya admin yang bisa masuk
            $key = $this->uri->segment(3);
            //cek apakah constraint
            $this->db->where('penerima',$key);
            $q2 = $this->db->get('disposisi');
            if($q2->num_rows()>0)
            {
                $this->session->set_flashdata('info','data masih digunakan oleh tabel lain ! silahkan gunakan opsi Non-aktifkan');
                redirect('pegawai');
            }
            $this->model_pegawai->getdelete($key);
            redirect('pegawai');
        }
        

}