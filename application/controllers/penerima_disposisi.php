<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penerima_disposisi extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->model_security->getsecurity();   //untuk sekuriti login
        $this->load->model('model_disposisi');
        $this->load->model('model_penerima');
        $this->load->model('model_ddm');
        $this->load->model('model_pegawai');
    }
        public function index($kode_disposisi)
    {
            $isi['content']     = 'penerima_disposisi/view_penerima';
            $isi['tabel']       = 'Daftar Penerima Disposisi';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";
            $isi['data']        = $this->model_penerima->view($kode_disposisi);
            $this->load->view('view_data_without_tambah',$isi);
    }

        
    public function state()
        {
            $key = $this->uri->segment(3);
            $disp = $this->uri->segment(4);
            $this->model_penerima->changeStatus($key);
            if($this->session->userdata('jabatan')=='administrator'){
                redirect('penerima_disposisi/index/'.$disp);
            }
                redirect('disposisi/mydisposisi');
        }
        

}