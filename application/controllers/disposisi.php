<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Disposisi extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->model_security->getsecurity();   //untuk sekuriti login
        $this->load->model('model_disposisi');
        $this->load->model('model_penerima');
        $this->load->model('model_ddm');
        $this->load->model('model_pegawai');
        $this->load->model('model_divisi');   
        $this->load->model('model_lembar'); 
    }
        public function index($kode_ddm)
    {
        $count = $this->model_disposisi->hakforAdd($kode_ddm);
            if($count->num_rows()==0 && $this->session->userdata('jabatan')=='pegawai'){
                redirect('home/Error404');
            }
            $isi['content']     = 'disposisi/view_disposisi';
            $isi['tabel']       = 'Disposisi';
            $isi['sub']         = 'Data';
            $isi['kode']        = $kode_ddm;
            $isi['data']        = $this->model_disposisi->view($kode_ddm);
            $isi['ddm']         = $this->model_ddm->getdata($kode_ddm);
            $this->load->view('view_data',$isi);
    }
        public function cetak($kode_ddm)
    {
        $count = $this->model_disposisi->hakforAdd($kode_ddm);
            if($count->num_rows()==0 && $this->session->userdata('jabatan')=='pegawai'){
                redirect('home/Error404');
            }
            $form_ygdigunakan = $this->model_lembar->getjenislembar($kode_ddm);
            if('internal'==$form_ygdigunakan->jenis_lembar){
                $isi['content']     = 'disposisi/lembar_internal';
            }else{
                $isi['content']     = 'disposisi/lembar_pusat';
            }
            $isi['tabel']       = 'Disposisi';
            $isi['sub']         = 'Data';
            $isi['kode']        = $kode_ddm;
            $isi['data']        = $this->model_disposisi->view($kode_ddm);
            $isi['ddm']         = $this->model_ddm->getdata($kode_ddm);
            $isi['divisi']      = $this->model_divisi->viewAktif();
            $this->load->view('invoice',$isi);
    }

        public function mydisposisi()
    {
            $isi['content']     = 'disposisi/view_mydisposisi';
            $isi['tabel']       = 'Disposisi';
            $isi['sub']         = 'Data';
            $isi['kode']        = '';
            $isi['data']        = $this->model_disposisi->getmydisposisi();
            $this->load->view('view_data',$isi);
    }
        public function tambah($kode_ddm)
        {

            //cek lagi deh nanti, kayaknya bakal error kalau udah ada disposisi & yg masuk secret
            $count = $this->model_disposisi->hakforAdd($kode_ddm);
            if($count->num_rows()==0 && $this->session->userdata('jabatan')=='pegawai'){
                redirect('home/Error404');
            }

            $isi['content']     = 'disposisi/form_disposisi';
            $isi['tabel']       = 'Disposisi';
            $isi['sub']         = 'Data';
            
            $isi['kode_disposisi']  = $this->model_disposisi->newID($kode_ddm);
            $isi['kode_ddm']  = $kode_ddm;
            $isi['old_dari'] = "";
            $isi['dari'] = $this->model_pegawai->view();
            $isi['old_penerima'] = "";
            $isi['penerima'] = $this->model_pegawai->viewAktif();
            $isi['tanggal'] = "";
            $isi['disposisi'] = "";
            $isi['pesan_singkat'] = "";
            $isi['show_lembar'] = $this->model_lembar->ifexist($kode_ddm);

            $this->load->view('form',$isi);
    
        }

        public function setformdisp($kode_ddm)
        {

            //cek lagi deh nanti, kayaknya bakal error kalau udah ada disposisi & yg masuk secret
            $count = $this->model_disposisi->hakforAdd($kode_ddm);
            if($count->num_rows()==0 && $this->session->userdata('jabatan')=='pegawai'){
                redirect('home/Error404');
            }
            
            $isi['content']     = 'disposisi/form_disposisi';
            $isi['tabel']       = 'Disposisi';
            $isi['sub']         = 'Data';
            
            $isi['kode_disposisi']  = $this->model_disposisi->newID($kode_ddm);
            $isi['kode_ddm']  = $kode_ddm;
            $isi['old_dari'] = "";
            $isi['dari'] = $this->model_pegawai->view();
            $isi['old_penerima'] = "";
            $isi['penerima'] = $this->model_pegawai->view();
            $isi['tanggal'] = "";
            $isi['disposisi'] = "";
            $isi['pesan_singkat'] = "";

            $this->load->view('form',$isi);
    
        }
        public function edit()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $isi['content']     = 'disposisi/form_disposisi';
            $isi['tabel']       = 'Disposisi';
            $isi['sub']         = 'Data';
            
            $key = $this->uri->segment(3);
            $this->db->where('kode_disposisi',$key);
            $query = $this->db->get('disposisi');
            if($query->num_rows()>0)
            {
                foreach ($query->result() as $row)
                {
                    $isi['kode_disposisi']  = $row->kode_disposisi;
                    $isi['kode_ddm']  = $row->kode_ddm;
                    $isi['old_dari'] = $row->dari;
                    $isi['dari'] = $this->model_pegawai->view();
                    $isi['old_penerima'] = $row->penerima;
                    $isi['penerima'] = $this->model_pegawai->view();
                    $isi['tanggal'] = $row->tanggal;
                    $isi['disposisi'] = $row->disposisi;
                    $isi['pesan_singkat'] = $row->pesan_singkat;
                }
            }
            else
            {
                    redirect('home/Error404');
            }
            $this->load->view('form',$isi);
    
        }

        
        public function simpan()
        {
            $key = $this->input->post('kode_disposisi');
            $ddm = $this->input->post('kode_ddm');
            //$imp = implode(',', $this->input->post('penerima'));
            $temp = $this->input->post('penerima');
            $lembar = $this->input->post('lembar');
            $data['kode_disposisi']   = $this->input->post('kode_disposisi');
            $data['kode_ddm']        = $this->input->post('kode_ddm');
            $data['dari']        = $this->input->post('dari');
            $data['tanggal']        = $this->input->post('tanggal');
            $data['disposisi']        = $this->input->post('disposisi');
            $data['pesan_singkat']        = $this->input->post('pesan_singkat');
            //menambahkan data di tabel lembar_disposisi
            if(!empty($lembar)){
                $this->model_lembar->getinsert($ddm,$lembar);    
            }
            $i=0;
            while($i < count($temp)){
                $x = $this->model_pegawai->getnama($temp[$i]); 
                foreach ($x->result() as $fill) {
                    $nama_peg[$i] = $fill->nama_pegawai;
                }
                $i++;
            }
            $imp = implode(', ', $nama_peg);
            $data['penerima']       = $imp;

            $query = $this->model_disposisi->getdata($key);
            if($query->num_rows()>0)
            {
                $this->model_disposisi->getupdate($key,$data);
                $this->session->set_flashdata('info','data suskses di update');
            }
            else
            {
                $this->model_disposisi->getinsert($data);
                //entri penerima disposisi, karena di tabel baru
                $this->model_penerima->getinsert($key,$temp);
                $this->session->set_flashdata('info','data suskses di simpan');
            }
            redirect('disposisi/index/'.$ddm);
            
        }
        public function delete()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $key = $this->uri->segment(3);
            $this->db->where('kode_disposisi',$key);
            $query = $this->db->get('disposisi');
            
            if($query->num_rows()>0)
            {
                $this->model_disposisi->getdelete($key);
            }
            $ddm = $this->model_disposisi->getKodeDdm($key);
            redirect('disposisi/index/'.$ddm);
        }

        public function notifme(){
            
                $data = array(
                    'username' => 1
                        );
            
            echo json_encode($data);
               
        }

        

}