<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ddm extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->model_security->getsecurity();   //untuk sekuriti login
        $this->load->model('model_ddm');
        $this->load->model('model_departemen');
        $this->load->model('model_grup');
        $this->load->model('model_divisi');
        $this->load->model('model_tahun_buku');
        $this->load->model('model_jenisdok');
        $this->load->model('model_pegawai');
    }
        public function index()
	{
            $isi['content']     = 'ddm/view_ddm';
            $isi['tabel']       = 'DDM';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";

            $isi['old_jenis']  = "";
            $isi['old_tahun']  = "";
            $isi['old_dept']  ="";
            $isi['my_dept']    = "";
            $isi['old_grup']  = "";
            $isi['old_divisi']  = "";
            $isi['data']        = $this->model_ddm->view();
            $isi['bydepartemen']  = $this->model_departemen->view();
            $isi['byjenisdok']  = $this->model_jenisdok->view();
            $isi['bytahun']  = $this->model_tahun_buku->view();
            if($this->session->userdata('jabatan')=='pegawai'){
                $this->load->view('view_data_without_tambah',$isi);    
            }else{
                $this->load->view('view_data',$isi);
            }
            
	}

    public function service()
    {
        $isi        = $this->model_ddm->view()->result();
        header('Content-Type: application/json');
        echo json_encode($isi);    
    }

    public function feed(){
        $this->load->helper(array('xml','text'));
        $data = array(
            'encoding'          => 'utf-8',
            'feed_name'         => 'Data Dokumen Masuk',
            'feed_url'          => 'http://sibi.b.go.id/index.php/ddm/feed/',
            'page_description'  => 'Cek dokumen terakhir yang masuk di DPSI',
            'page_language'     => 'en-ca',
            'creator_email'     => 'hendraprasetio1995@gmail.com',
            'posts'             => $this->model_ddm->view(1)
        );


        header("Content-Type: application/rss+xml");
        $this->load->vars($data);
        $this->load->view('features/rss_ddm');
    }

        public function bysatker  ()
    {
            $dept = $_GET['departemen'];
            $grup = $_GET['grup'];
            $divisi = $_GET['divisi'];
            $jenis = "";
            $tahun = "";
            $isi['content']     = 'ddm/view_ddm';
            $isi['tabel']       = 'DDM';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";
            
            $isi['old_jenis']  = $jenis;
            $isi['old_tahun']  = $tahun;
            $isi['my_dept']    = "";
            $isi['old_dept']  = $dept;
            $isi['old_grup']  = $grup;
            $isi['old_divisi']  = $divisi;
            $isi['data']        = $this->model_ddm->viewbyfilter($jenis,$tahun,$dept,$grup,$divisi);
            $isi['bydepartemen']  = $this->model_departemen->view();
            $isi['byjenisdok']  = $this->model_jenisdok->view();
            $isi['bytahun']  = $this->model_tahun_buku->view();
            if($this->session->userdata('jabatan')=='pegawai'){
                $this->load->view('view_data_without_tambah',$isi);    
            }else{
                $this->load->view('view_data',$isi);
            }
    }
        public function filter  ()
    {

            $jenis = $this->input->post('jenis');
            $tahun = $this->input->post('tahun');
            $combo_dept = $this->input->post('departemen');
            if(!empty($combo_dept)){
                $dept = $combo_dept;
                $grup = "";
                $divisi = "";
            }else{
                $dept = $_GET['departemen'];
                $grup = $_GET['grup'];
                $divisi = $_GET['divisi'];       
            }
            $isi['content']     = 'ddm/view_ddm';
            $isi['tabel']       = 'DDM';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";
            
            $isi['old_jenis']  = $jenis;
            $isi['old_tahun']  = $tahun;
            $isi['my_dept']    = $dept;
            $isi['old_dept']  = $dept;
            $isi['old_grup']  = $grup;
            $isi['old_divisi']  = $divisi;
            $isi['data']        = $this->model_ddm->viewbyfilter($jenis,$tahun,$dept,$grup,$divisi);
            $isi['byjenisdok']  = $this->model_jenisdok->view();
            $isi['bytahun']  = $this->model_tahun_buku->view();
            $isi['bydepartemen']  = $this->model_departemen->view();
            if($this->session->userdata('jabatan')=='pegawai'){
                $this->load->view('view_data_without_tambah',$isi);    
            }else{
                $this->load->view('view_data',$isi);
            }
    }
        public function cetak()
    {
            $jenis = $_GET['jenis'];
            $tahun = $_GET['tahun'];
            $dept = $_GET['departemen'];
            $grup = $_GET['grup'];
            $divisi = $_GET['divisi'];
            $isi['content']     = 'ddm/invoice_ddm';
            $isi['tabel']       = 'DDM';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";

            $isi['jenis']  = $jenis;
            $isi['tahun']  = $tahun;
            $isi['dept']  = $dept;
            $isi['grup']  = $grup;
            $isi['divisi']  = $divisi;
            $isi['data']        = $this->model_ddm->viewbyfilter($jenis,$tahun,$dept,$grup,$divisi);
            $isi['pegawai']     = $this->model_pegawai->getdata($this->session->userdata('nip'));
            $this->load->view('invoice',$isi);
    }
        public function tambah()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $isi['content']     = 'ddm/form_ddm';
            $isi['tabel']       = 'DDM';
            $isi['sub']         = 'Data';
            
            $isi['kode_ddm']  = $this->model_ddm->newID();
            $isi['nomor_dokumen']  = "";
            $isi['tanggal_dokumen'] = "";
            $isi['tanggal_masuk'] = "";
            $isi['perihal'] = "";
            $isi['ditujukan_kepada'] = "";
            $isi['keterangan']  = "";

            $this->load->view('form',$isi);
    
        }
        public function edit()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $isi['content']     = 'ddm/edit_ddm';
            $isi['tabel']       = 'DDM';
            $isi['sub']         = 'Data';
            
            $key = $this->uri->segment(3);
            $this->db->where('kode_ddm',$key);
            $query = $this->db->get('ddm');
            if($query->num_rows()>0)
            {
                foreach ($query->result() as $row)
                {
                    $isi['kode_ddm'] = $row->kode_ddm;
                    $isi['nomor_dokumen']  = $row->nomor_dokumen;
                    $isi['tanggal_dokumen'] = $row->tanggal_dokumen;
                    $isi['tanggal_masuk'] = $row->tanggal_masuk;
                    $isi['perihal'] = $row->perihal;
                    $isi['ditujukan_kepada'] = $row->ditujukan_kepada;
                    $isi['keterangan']  = $row->keterangan;
                }
            }
            else
            {
                redirect('home/Error404');
            }
            $this->load->view('form',$isi);
    
        }

        public function checkId(){
            $id = $this->input->post('id');
            $this->db->where('nomor_dokumen',$id);
            $query = $this->db->get('ddm');
            $cek = explode('/', $id);
            if(count($cek)!=5){
                $data = array(
                        'username' => 0
                            );
            }
            else{

                if($query->num_rows()>0){
                    $data = array(
                        'username' => 0
                            );
                }
                else {
                    $data = array(
                        'username' => 1
                            );
                }
            }
            echo json_encode($data);
        }
        public function simpan()
        {
            $key = $this->input->post('kode_ddm');
            $nomor = $this->input->post('nomor_dokumen');
            $data['kode_ddm']   = $this->input->post('kode_ddm');
            $data['nomor_dokumen']        = $this->input->post('nomor_dokumen');
            $data['tanggal_dokumen']        = $this->input->post('tanggal_dokumen');
            $data['tanggal_masuk']        = $this->input->post('tanggal_masuk');
            $data['perihal']        = $this->input->post('perihal');
            $data['ditujukan_kepada']        = $this->input->post('ditujukan_kepada');
            $data['keterangan']        = $this->input->post('keterangan');
            
            $query = $this->model_ddm->getdata($key);
            if($query->num_rows()>0)
            {
                $this->model_ddm->getupdate($key,$data);
                $this->session->set_flashdata('info','data suskses di update');
            }
            else
            {
                //cek nomor dokumen
                $check = $this->model_ddm->getnomor($nomor);
                if($check->num_rows()>0){
                    $this->session->set_flashdata('info','nomor '.$nomor.' sudah digunakan sebelumnya');
                    redirect('ddm');
                }

                //simpan
                $this->model_ddm->getinsert($data);
                $this->session->set_flashdata('info','data suskses di simpan');
            }
            redirect('ddm');
            
        }
        public function delete()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $key = $this->uri->segment(3);
            $this->db->where('kode_ddm',$key);
            $query = $this->db->get('ddm');
            
            if($query->num_rows()>0)
            {
                $this->model_ddm->getdelete($key);
            }
            redirect('ddm');
        }

}