<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Features extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->model_security->getsecurity();   //untuk sekuriti login
        $this->model_security->getlevel();   //hanya admin yang bisa masuk
    }
        public function rss()
	{
            $isi['content']     = 'features/rss.html';
            $isi['tabel']       = 'Rss';
            $isi['sub']         = 'Info';
            $isi['kode']        = "";
            $isi['data']        = '';
            $this->load->view('features/rss.xml',$isi);
	}

    public function rssgoal()
    {
        $isi['content']     = 'features/rss_pens';
        $isi['judul']       = 'Dashboard';
        $isi['sub_judul']   = 'Overview';
        $this->load->library('rssparser');                          // load library
        $this->rssparser->set_feed_url('http://www.goal.com/id-ID/feeds/news?id=1387&fmt=rss&ICID=AR');  // get feed
        $this->rssparser->set_cache_life(30);                       // Set cache life time in minutes
        $isi['rss'] = $this->rssparser->getFeed(6);                        // Get six items from the feed
        $this->load->view('view_home',$isi);
    }

    public function podcast()
    {
        $this->load->helper(array('xml','text'));
        $data = array(
            'encoding'          => 'utf-8',
            'feed_name'         => 'Podcast',
            'feed_url'          => 'http://sibi.bi.go.id/index.php/features/podcast',
            'page_description'  => 'Get podcast Here',
            'page_language'     => 'en-ca',
            'creator_email'     => 'hendraprasetio1995@gmail.com',
        );
        header("Content-Type: application/rss+xml");
        $this->load->view('features/podcast',$data);
    }

}