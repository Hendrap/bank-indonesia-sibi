<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grup extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->model_security->getsecurity();   //untuk sekuriti login
        $this->load->model('model_grup');
        $this->load->model('model_departemen');
    }
        public function index()
	{
            $this->model_security->justadmin();   //hanya admin yang bisa masuk
            $isi['content']     = 'grup/view_grup';
            $isi['tabel']       = 'Grup';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";
            $isi['data']        = $this->model_grup->view();
            $this->load->view('view_data',$isi);
	}
        public function tambah()
        {
            $this->model_security->justadmin();   //hanya admin yang bisa masuk
            $isi['content']     = 'grup/form_grup';
            $isi['tabel']       = 'Grup';
            $isi['sub']         = 'Data';

            $isi['kode_grup'] = "";
            $isi['nama_grup'] = "";
            $isi['kode_departemen'] = $this->model_departemen->viewAktif();
            $isi['old_kode_departemen'] = "";
            $isi['aktif'] = "";
            $this->load->view('form',$isi);
    
        }
        
        public function edit()
        {
            $this->model_security->justadmin();   //hanya admin yang bisa masuk
            $isi['content']     = 'grup/edit_grup';
            $isi['tabel']       = 'Grup';
            $isi['sub']         = 'Data';
            
            $key = $this->uri->segment(3);
            $this->db->where('kode_grup',$key);
            $query = $this->db->get('grup');
            if($query->num_rows()>0)
            {
                foreach ($query->result() as $row)
                {
                    $isi['kode_grup'] = $row->kode_grup;
                    $isi['nama_grup'] = $row->nama_grup;
                    $isi['kode_departemen'] = $this->model_departemen->viewAktif();
                    $isi['old_kode_departemen'] = $row->kode_departemen;
                    $isi['aktif'] = $row->aktif;
                }
            }
            else
            {
                    redirect('home/Error404');
            }
            $this->load->view('form',$isi);
    
        }
        public function checkId(){
            $id = $this->input->post('id');
            $this->db->where('kode_grup',$id);
            $query = $this->db->get('grup');
            
            if($query->num_rows()>0){
                $data = array(
                    'username' => 0
                        );
            }
            else {
                $data = array(
                    'username' => 1
                        );
            }
            echo json_encode($data);
        }
        public function simpan()
        {
            $key = $this->input->post('kode_grup');
            $data['kode_grup']   = $this->input->post('kode_grup');
            $data['nama_grup']        = $this->input->post('nama_grup');
            $data['kode_departemen']   = $this->input->post('kode_departemen');
            $data['aktif']   = 1;
        
            $query = $this->model_grup->getdata($key);
            if($query->num_rows()>0)
            {
                $this->model_grup->getupdate($key,$data);
                $this->session->set_flashdata('info','data suskses di update');
            }
            else
            {
                $this->model_grup->getinsert($data);
                $this->session->set_flashdata('info','data suskses di simpan');
            }
            redirect('grup');
            
        }
        public function state()
        {
            $this->model_security->justadmin();   //hanya admin yang bisa masuk
            $key = $this->uri->segment(3);
            $stat = $this->uri->segment(4);
            $this->db->where('kode_grup',$key);
            $query = $this->db->get('grup');
            
            if($query->num_rows()>0)
            {
                $this->model_grup->changeStatus($key,$stat);
            }
            redirect('grup');
        }
        public function delete()
        {
            $this->model_security->justadmin();   //hanya admin yang bisa masuk
            $key = $this->uri->segment(3);
            //cek apakah constraint
            $this->db->where('kode_grup',$key);
            $q2 = $this->db->get('divisi');
            if($q2->num_rows()>0)
            {
                $this->session->set_flashdata('info','(Constraint) Gagal ! data masih digunakan oleh tabel lain. ');
                redirect('grup');
            }
            //delete data
            $this->db->where('kode_grup',$key);
            $query = $this->db->get('grup');
            
            if($query->num_rows()>0)
            {
                $this->model_grup->getdelete($key);
            }
            redirect('grup');
        }

}