<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenis_dokumen extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->model_security->getsecurity();   //untuk sekuriti login
        $this->model_security->getlevel();   //hanya admin yang bisa masuk
        $this->load->model('model_jenis');
    }
        public function index()
	{
            $isi['content']     = 'jenis_dokumen/view_jenis';
            $isi['tabel']       = 'Jenis_dokumen';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";
            $isi['data']        = $this->model_jenis->view();
            $this->load->view('view_data',$isi);
	}
        public function tambah()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $isi['content']     = 'jenis_dokumen/form_jenis';
            $isi['tabel']       = 'Jenis_dokumen';
            $isi['sub']         = 'Data';

            $isi['kode_jenisdok'] = "";
            $isi['jenis_dokumen'] = "";
            $this->load->view('form',$isi);
    
        }
        public function checkId(){
            $id = $this->input->post('id');
            $this->db->where('kode_jenisdok',$id);
            $query = $this->db->get('jenis_dokumen');
            
            if($query->num_rows()>0){
                $data = array(
                    'username' => 0
                        );
            }
            else {
                $data = array(
                    'username' => 1
                        );
            }
            echo json_encode($data);
        }
        public function edit()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $isi['content']     = 'jenis_dokumen/edit_jenis';
            $isi['tabel']       = 'Jenis_dokumen';
            $isi['sub']         = 'Data';
            
            $key = $this->uri->segment(3);
            $this->db->where('kode_jenisdok',$key);
            $query = $this->db->get('jenis_dokumen');
            if($query->num_rows()>0)
            {
                foreach ($query->result() as $row)
                {
                    $isi['kode_jenisdok'] = $row->kode_jenisdok;
                    $isi['jenis_dokumen'] = $row->jenis_dokumen;
                }
            }
            else
            {
                    redirect('home/Error404');
            }
            $this->load->view('form',$isi);
    
        }
        
        public function simpan()
        {
            $key = $this->input->post('kode_jenisdok');
            $data['kode_jenisdok']   = $this->input->post('kode_jenisdok');
            $data['jenis_dokumen']        = $this->input->post('jenis_dokumen');
        
            $query = $this->model_jenis->getdata($key);
            if($query->num_rows()>0)
            {
                $this->model_jenis->getupdate($key,$data);
                $this->session->set_flashdata('info','data suskses di update');
            }
            else
            {
                $this->model_jenis->getinsert($data);
                $this->session->set_flashdata('info','data suskses di simpan');
            }
            redirect('jenis_dokumen');
            
        }
        public function delete()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $key = $this->uri->segment(3);
            //cek apakah constraint
            $this->db->where('kode_jenisdok',$key);
            $q2 = $this->db->get('ddk');
            if($q2->num_rows()>0)
            {
                $this->session->set_flashdata('info','(Constraint) Gagal ! data masih digunakan oleh tabel lain.');
                redirect('jenis_dokumen');
            }
            //delete data
            $this->db->where('kode_jenisdok',$key);
            $query = $this->db->get('jenis_dokumen');
            
            if($query->num_rows()>0)
            {
                $this->model_jenis->getdelete($key);
            }
            redirect('jenis_dokumen');
        }

}