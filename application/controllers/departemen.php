<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Departemen extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->model_security->getsecurity();   //untuk sekuriti login
        $this->model_security->getlevel();   //hanya admin yang bisa masuk
        $this->load->model('model_departemen');
    }
        public function index()
	{
        $this->model_security->justadmin();   //hanya admin yang bisa masuk
            $isi['content']     = 'departemen/view_departemen';
            $isi['tabel']       = 'Departemen';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";
            $isi['data']        = $this->model_departemen->view();
            $this->load->view('view_data',$isi);
	}
        public function tambah()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $isi['content']     = 'departemen/form_departemen';
            $isi['tabel']       = 'Departemen';
            $isi['sub']         = 'Data';

            $isi['kode_departemen'] = "";
            $isi['nama_departemen'] = "";
            $isi['aktif'] = "";
            $this->load->view('form',$isi);
    
        }
        
        public function edit()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $isi['content']     = 'departemen/edit_departemen';
            $isi['tabel']       = 'Departemen';
            $isi['sub']         = 'Data';
            
            $key = $this->uri->segment(3);
            $this->db->where('kode_departemen',$key);
            $query = $this->db->get('departemen');
            if($query->num_rows()>0)
            {
                foreach ($query->result() as $row)
                {
                    $isi['kode_departemen'] = $row->kode_departemen;
                    $isi['nama_departemen'] = $row->nama_departemen;
                    $isi['aktif'] = $row->aktif;
                }
            }
            else
            {
                    redirect('home/Error404');
            }
            $this->load->view('form',$isi);
    
        }
        public function checkId(){
            $id = $this->input->post('id');
            $this->db->where('kode_departemen',$id);
            $query = $this->db->get('departemen');
            
            if($query->num_rows()>0){
                $data = array(
                    'username' => 0
                        );
            }
            else {
                $data = array(
                    'username' => 1
                        );
            }
            echo json_encode($data);
        }
        public function simpan()
        {
            $key = $this->input->post('kode_departemen');
            $data['kode_departemen']   = $this->input->post('kode_departemen');
            $data['nama_departemen']        = $this->input->post('nama_departemen');
            $data['aktif']   = 1;
        
            $query = $this->model_departemen->getdata($key);
            if($query->num_rows()>0)
            {
                $this->model_departemen->getupdate($key,$data);
                $this->session->set_flashdata('info','data suskses di update');
            }
            else
            {
                $this->model_departemen->getinsert($data);
                $this->session->set_flashdata('info','data suskses di simpan');
            }
            redirect('departemen');
            
        }
        public function state()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $key = $this->uri->segment(3);
            $stat = $this->uri->segment(4);
            $this->db->where('kode_departemen',$key);
            $query = $this->db->get('departemen');
            
            if($query->num_rows()>0)
            {
                $this->model_departemen->changeStatus($key,$stat);
            }
            redirect('departemen');
        }
        public function delete()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $key = $this->uri->segment(3);
            //cek apakah constraint
            $this->db->where('kode_departemen',$key);
            $q2 = $this->db->get('divisi');
            if($q2->num_rows()>0)
            {
                $this->session->set_flashdata('info','(Constraint) Gagal ! data masih digunakan oleh tabel lain.');
                redirect('departemen');
            }
            //delete data
            $this->db->where('kode_departemen',$key);
            $query = $this->db->get('departemen');
            
            if($query->num_rows()>0)
            {
                $this->model_departemen->getdelete($key);
            }
            redirect('departemen');
        }

}