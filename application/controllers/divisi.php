<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Divisi extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->model_security->getsecurity();   //untuk sekuriti login
        $this->model_security->justadmin();   //hanya admin yang bisa masuk
        $this->load->model('model_divisi');
        $this->load->model('model_departemen');
        $this->load->model('model_grup');
    }
        public function index()
	{
            $this->model_security->justadmin();   //hanya admin yang bisa masuk
            $isi['content']     = 'divisi/view_divisi';
            $isi['tabel']       = 'Divisi';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";
            $isi['data']        = $this->model_divisi->view();
            $this->load->view('view_data',$isi);
	}
        public function tambah()
        {
            $this->model_security->justadmin();   //hanya admin yang bisa masuk
            $isi['content']     = 'divisi/form_divisi';
            $isi['tabel']       = 'Divisi';
            $isi['sub']         = 'Data';

            $isi['kode_divisi'] = "";
            $isi['nama_divisi'] = "";
            $isi['kode_departemen'] = $this->model_departemen->viewAktif();
            $isi['old_kode_departemen'] = "";
            $isi['kode_grup'] = $this->model_grup->viewAktif();
            $isi['old_kode_grup'] = "";
            $isi['aktif'] = "";
            $this->load->view('form',$isi);
    
        }
        public function checkId(){
            $id = $this->input->post('id');
            $this->db->where('kode_divisi',$id);
            $query = $this->db->get('divisi');
            
            if($query->num_rows()>0){
                $data = array(
                    'username' => 0
                        );
            }
            else {
                $data = array(
                    'username' => 1
                        );
            }
            echo json_encode($data);
        }
        public function edit()
        {
            $this->model_security->justadmin();   //hanya admin yang bisa masuk
            $isi['content']     = 'divisi/edit_divisi';
            $isi['tabel']       = 'Divisi';
            $isi['sub']         = 'Data';
            
            $key = $this->uri->segment(3);
            $this->db->where('kode_divisi',$key);
            $query = $this->db->get('divisi');
            if($query->num_rows()>0)
            {
                foreach ($query->result() as $row)
                {
                    $isi['kode_divisi'] = $row->kode_divisi;
                    $isi['nama_divisi'] = $row->nama_divisi;
                    $isi['kode_departemen'] = $this->model_departemen->viewAktif();
                    $isi['old_kode_departemen'] = $row->kode_departemen;
                    $isi['kode_grup'] = $this->model_grup->viewAktif();
                    $isi['old_kode_grup'] = $row->kode_grup;
                    $isi['aktif'] = $row->aktif;
                }
            }
            else
            {
                    redirect('home/Error404');
            }
            $this->load->view('form',$isi);
    
        }
        
        public function simpan()
        {
            $key = $this->input->post('kode_divisi');
            $data['kode_divisi']   = $this->input->post('kode_divisi');
            $data['nama_divisi']        = $this->input->post('nama_divisi');
            $data['kode_departemen']   = $this->input->post('kode_departemen');
            $data['kode_grup']   = $this->input->post('kode_grup');
            $data['aktif']   = 1;
        
            $query = $this->model_divisi->getdata($key);
            if($query->num_rows()>0)
            {
                $this->model_divisi->getupdate($key,$data);
                $this->session->set_flashdata('info','data suskses di update');
            }
            else
            {
                $this->model_divisi->getinsert($data);
                $this->session->set_flashdata('info','data suskses di simpan');
            }
            redirect('divisi');
            
        }
        public function state()
        {
            $this->model_security->justadmin();   //hanya admin yang bisa masuk
            $key = $this->uri->segment(3);
            $stat = $this->uri->segment(4);
            $this->db->where('kode_divisi',$key);
            $query = $this->db->get('divisi');
            
            if($query->num_rows()>0)
            {
                $this->model_divisi->changeStatus($key,$stat);
            }
            redirect('divisi');
        }
        public function delete()
        {
            $this->model_security->justadmin();   //hanya admin yang bisa masuk
            $key = $this->uri->segment(3);
            //cek apakah constraint
            $this->db->where('kode_divisi',$key);
            $q2 = $this->db->get('pegawai');
            if($q2->num_rows()>0)
            {
                $this->session->set_flashdata('info','(Constraint) Gagal ! data masih digunakan oleh tabel lain. ');
                redirect('divisi');
            }
            //delete data
            $this->db->where('kode_divisi',$key);
            $query = $this->db->get('divisi');
            
            if($query->num_rows()>0)
            {
                $this->model_divisi->getdelete($key);
            }
            redirect('divisi');
        }

}