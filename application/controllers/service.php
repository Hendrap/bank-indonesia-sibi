<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service extends CI_Controller {

    public function auth()
    {
        $u = $this->input->post('username');
        $p = $this->input->post('password');
        $this->load->model('model_login');
        $result = $this->model_login->apilogin($u,$p);
        return json_encode(array('haha'=>$result));
    }
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */