<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->model_security->getsecurity();   //untuk sekuriti login
        $this->load->model('model_home');
        $this->load->model('model_pegawai');
        $this->load->model('model_ddm');
        $this->load->model('model_ddk');
        $this->load->model('model_disposisi');
    }
	public function index()
	{
            $isi['content']     = 'home/content';
            $isi['judul']       = 'Dashboard';
            $isi['sub_judul']   = 'Overview';
            $isi['c_pegawai']   = $this->model_pegawai->jumlah_data();
            $isi['c_ddk']   = $this->model_ddk->jumlah_data();
            $isi['c_ddm']   = $this->model_ddm->jumlah_data();
            $isi['c_disposisi']   = $this->model_disposisi->jumlah_data();
            $this->load->view('view_home',$isi);
	}
    
    public function Error404()
    {
            $isi['content']     = 'home/404';
            $isi['judul']       = 'Dashboard';
            $isi['sub_judul']   = '404';
            $this->load->view('view_home',$isi);
    } 
    public function logout()
    {
            $this->session->sess_destroy();
            redirect('login');
    }
}
