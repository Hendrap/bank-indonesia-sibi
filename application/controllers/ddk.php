<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ddk extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->model_security->getsecurity();   //untuk sekuriti login
        $this->load->model('model_ddk');
        $this->load->model('model_departemen');
        $this->load->model('model_grup');
        $this->load->model('model_divisi');
        $this->load->model('model_tahun_buku');
        $this->load->model('model_jenisdok');
        $this->load->model('model_pegawai');
    }
        public function index()
	{
            $isi['content']     = 'ddk/view_ddk';
            $isi['tabel']       = 'DDK';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";


            $isi['old_jenis']  = "";
            $isi['old_tahun']  = "";
            $isi['data']        = $this->model_ddk->view();
            $isi['byjenisdok']  = $this->model_jenisdok->view();
            $isi['bytahun']  = $this->model_tahun_buku->view();
            if($this->session->userdata('jabatan')=='pegawai'){
                $this->load->view('view_data_without_tambah',$isi);    
            }else{
                $this->load->view('view_data',$isi);
            }
	}
        public function view($key)
    {
            $isi['content']     = 'ddk/detail_ddk';
            $isi['tabel']       = 'Detail Dokumen Keluar';
            $isi['sub']         = 'Data';
            $data = $this->model_ddk->getdata($key);
            foreach ($data->result() as $row) {
                $isi['kode_ddm']    = $row->kode_ddk;
                $isi['nomor_dokumen']    = $row->nomor_dokumen;
                $isi['tanggal_dokumen']    = $row->tanggal_dokumen;
                $isi['perihal']    = $row->perihal;
                $isi['pemrakarsa']    = $row->pemrakarsa;
                $isi['ditujukan_kepada']    = $row->ditujukan_kepada;
                $isi['keterangan']    = $row->keterangan;
            }
            $this->load->view('form',$isi);
    }
        public function filter  ()
    {

            $jenis = $this->input->post('jenis');
            $tahun = $this->input->post('tahun');
            
            $isi['content']     = 'ddk/view_ddk';
            $isi['tabel']       = 'DDK';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";
            
            $isi['old_jenis']  = $jenis;
            $isi['old_tahun']  = $tahun;
            $isi['data']        = $this->model_ddk->viewbyfilter($jenis,$tahun);
            $isi['byjenisdok']  = $this->model_jenisdok->view();
            $isi['bytahun']  = $this->model_tahun_buku->view();
            if($this->session->userdata('jabatan')=='pegawai'){
                $this->load->view('view_data_without_tambah',$isi);    
            }else{
                $this->load->view('view_data',$isi);
            }
    }
        public function cetak()
    {
            $jenis = $_GET['jenis'];
            $tahun = $_GET['tahun'];
            
            $isi['content']     = 'ddk/invoice_ddk';
            $isi['tabel']       = 'DDK';
            $isi['sub']         = 'Data';
            $isi['kode']        = "";
            
            $isi['jenis']  = $jenis;
            $isi['tahun']  = $tahun;
            $isi['data']        = $this->model_ddk->viewbyfilter($jenis,$tahun);
            $isi['pegawai']     = $this->model_pegawai->getdata($this->session->userdata('nip'));
            $this->load->view('invoice',$isi);
    }
        public function tambah()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $isi['content']     = 'ddk/form_ddk';
            $isi['tabel']       = 'DDK';
            $isi['sub']         = 'Data';
            
            $isi['kode_ddk']  = $this->model_ddk->newID();
            $isi['nomor_dokumen']  = "";
            $isi['tanggal_dokumen'] = "";
            $isi['no_sesuai_jenis'] = "";
            $isi['old_kode_divisi'] = "";
            $isi['perihal'] = "";
            $isi['old_rahasia'] = "";
            $isi['old_kode_tahun']  = "";
            $isi['old_kode_jenisdok'] = "";
            $isi['ditujukan_kepada'] = "";
            $isi['kode_departemen'] = $this->model_departemen->viewAktif();
            $isi['kode_divisi'] = $this->model_divisi->getOrder();
            $isi['kode_divisi_pe'] = $this->model_divisi->getOrder();
            $isi['kode_grup'] = $this->model_grup->getOrder();
            $isi['kode_grup_pe'] = $this->model_grup->getOrder();
            $isi['pemrakarsa'] = "";
            $isi['kode_tahun']  = $this->model_tahun_buku->view();
            $isi['kode_jenisdok'] = $this->model_jenisdok->view();
            $isi['rahasia']  = "";
            $isi['keterangan']  = "";

            $isi['info_jenis'] =$this->model_ddk->getInfoJenis();
            $this->load->view('form',$isi);
    
        }
        public function edit()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $isi['content']     = 'ddk/edit_ddk';
            $isi['tabel']       = 'DDK';
            $isi['sub']         = 'Data';
            

            $isi['info_jenis'] =$this->model_ddk->getInfoJenis();
            $key = $this->uri->segment(3);
            $this->db->where('kode_ddk',$key);
            $query = $this->db->get('ddk');
            if($query->num_rows()>0)
            {
                foreach ($query->result() as $row)
                {
                    $isi['kode_ddk'] = $row->kode_ddk;
                    $isi['nomor_dokumen'] = $row->nomor_dokumen;
                    $isi['no_sesuai_jenis'] = $row->no_sesuai_jenis;
                    $isi['tanggal_dokumen'] = $row->tanggal_dokumen;
                    $isi['perihal'] = $row->perihal;
                    $isi['old_kode_tahun'] = $row->kode_tahun;
                    $isi['old_rahasia'] = $row->rahasia;
                    $isi['old_kode_jenisdok'] = $row->kode_jenisdok;
                    $isi['ditujukan_kepada'] = $row->ditujukan_kepada;
                    $isi['pemrakarsa'] = $row->pemrakarsa;
                    $isi['kode_departemen'] = $this->model_departemen->viewAktif();
                    $isi['kode_grup'] = $this->model_grup->viewAktif();
                    $isi['kode_divisi'] = $this->model_divisi->viewAktif();
                    $isi['kode_tahun']  = $this->model_tahun_buku->view();
                    $isi['kode_jenisdok'] = $this->model_jenisdok->view();
                    $isi['keterangan']  = $row->keterangan;
                }
            }
            else
            {
                    redirect('home/Error404');
            }
            $this->load->view('form',$isi);
    
        }

        
        public function simpan()
        {
            $key = $this->input->post('kode_ddk');
            $data['kode_ddk']   = $this->input->post('kode_ddk');
            $data['nomor_dokumen']        = $this->input->post('nomor_dokumen');
            $data['no_sesuai_jenis']        = $this->input->post('no_sesuai_jenis');
            $data['tanggal_dokumen']        = $this->input->post('tanggal_dokumen');
            $data['perihal']        = $this->input->post('perihal');
            $data['kode_tahun']        = $this->input->post('kode_tahun');
            $data['pemrakarsa']        = $this->input->post('pemrakarsa');
            $data['rahasia']        = $this->input->post('rahasia');
            $data['kode_jenisdok']        = $this->input->post('kode_jenisdok');
            $data['ditujukan_kepada']        = $this->input->post('ditujukan_kepada');
            $data['keterangan']        = $this->input->post('keterangan');
            
            $query = $this->model_ddk->getdata($key);
            if($query->num_rows()>0)
            {
                $this->model_ddk->getupdate($key,$data);
                $this->session->set_flashdata('info','data suskses di update');
            }
            else
            {
                $this->model_ddk->getinsert($data);
                $this->session->set_flashdata('info','data suskses di simpan');
            }
            redirect('ddk');
            
        }
        public function delete()
        {
            $this->model_security->getlevel();   //hanya admin yang bisa masuk
            $key = $this->uri->segment(3);
            $this->db->where('kode_ddk',$key);
            $query = $this->db->get('ddk');
            
            if($query->num_rows()>0)
            {
                $this->model_ddk->getdelete($key);
            }
            redirect('ddk');
        }

        public function checkNomor(){
            $id = $this->input->post('id');
            $this->db->where('nomor_dokumen',$id);
            $query = $this->db->get('ddk');
            
            if($query->num_rows()>0){
                $data = array(
                    'result' => 0
                        );
            }
            else {
                $data = array(
                    'result' => 1
                        );
            }
            echo json_encode($data);
        }

        public function getgroup(){
            if($_REQUEST)
            {
                $id     = $_REQUEST['parent_id'];
                $temp = $this->db->query("SELECT * FROM grup where kode_departemen = '$id'");
                ?>
                <select name="grup"  id="grup_id">
                <option value="" selected="selected"></option>
                <?php
                foreach ($temp->result() as $rows) { ?>
                    <option value="<?php echo $rows->kode_grup;?>"  ID="<?php echo $rows->kode_grup;?>"><?php echo $rows->nama_grup;?></option>
                <?php } ?>
                </select>       
            <?php }
        }

}