<style type="text/css">
table{
  width: 100%;
   border: 1px solid black;
}
thead{
  border-bottom: 1px solid black;
}
th{
  text-align: center;
  border: 1px solid black;
}
td {
    padding: 15px;
    text-align: left;
   border: 1px solid black;
} 

.mytable{
}
.mytable td{
    vertical-align: text-top;
}
.tengahin td{
  text-align :center;
}
.logo img{
  width: 20px;
}
</style>
<section class="invoice">
    <div class="row" align="left">
      <div class="margin">
        <img src="<?php echo base_url(); ?>/assets/dist/img/logo-name.png" width="200" />
      </div>
    </div>
    <!-- title row -->
    <div class="row" align="center">
      <div class="col-xs-12">
        <h2 class="page-header">LEMBAR DISPOSISI PEJABAT</h2>
      </div>
    </div>
    
    <div class="row">
      <div class="col-xs-12 table-responsive">
                  <table> 
                    <?php foreach ($ddm->result() as $ddm) { ?>
                      <tr>
                        <td width="50%">No/Tanggal : <?php echo $ddm->nomor_dokumen; echo "     (".$ddm->tanggal_dokumen.")";?></td>
                        <td width="50%">Asal Dokumen : <?php echo $ddm->pemrakarsa; ?></td>
                      </tr>
                      <tr>
                        <td>Perihal : <?php echo $ddm->perihal;?></td>
                        <td>Tanggal Terima : <?php echo $ddm->tanggal_masuk;?></td>
                      </tr>
                      <tr class="tengahin"><td colspan="2">
                          <label><input type="checkbox">Biasa</label>
                          <label><input type="checkbox">Sangat Segera</label>
                          <label><input type="checkbox">Segera</label>
                      </td></tr>
                      <tr><td colspan="2">Dari : </td></tr>
                      <tr><td colspan="2">DISPOSISI KEPADA :</td></tr>
                    <?php } ?>
                  </table>
      </div>
    </div>
    <table class="mytable">
    <tr>
          <td>
            <strong>Ketua Tim</strong>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox">
                          Ibu Itjut Noerhajati
                        </label>
                      </div>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox">
                          Bp. Deni Mulyosadhono
                        </label>
                      </div>
          </td>
          <td>
            <strong>Divisi MPSI</strong>
                      <div class="checkbox"><label><input type="checkbox"> Bp. Indra A</label></div>
                      <div class="checkbox"><label><input type="checkbox"> Bp. Syarief</label></div>
                      <div class="checkbox"><label><input type="checkbox"> Bp. Bahari Priyatna</label></div>
                      <div class="checkbox"><label><input type="checkbox"> Ibu Martha D.s</label></div>
                      <div class="checkbox"><label><input type="checkbox"></label></div>
                      <div class="checkbox"><label><input type="checkbox"></label></div>
                      <div class="checkbox"><label><input type="checkbox"></label></div>
          </td>
          <td>
            <strong>Petunjuk/Disposisi</strong>
            <div class="checkbox"><label><input type="checkbox">Setuju</label></div>
            <div class="checkbox"><label><input type="checkbox">Tolak</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk diteliti dan pendapat</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk diketahui</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk diselenggarakan sesuai ketentuan yang berlaku</label></div>
            <div class="checkbox"><label><input type="checkbox">Sesuai catatan</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk perhatian</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk diedarkan</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk dijawab</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk diperbaiki</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk dibicarakan dengan saya</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk dibicarakan bersama</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk diingatkan</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk disimpan</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk disiapkan</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk dijadwalkan</label></div>
            <div class="checkbox"><label><input type="checkbox">Untuk dihadiri/diwakili</label></div>
            <div class="checkbox"><label><input type="checkbox">Dokumen asli kepada ...</label></div>
          </td>
    </tr>
    </table>
    <!-- /.row -->

  <div class="row">
    <div class="row" align="left">
      <div class="col-xs-12">

      <div class="margin">
        <h2 class="page-header">DISPOSISI DOKUMEN</h2>
      </div>
      </div>
    </div>
  </div>
    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
                  <table id="example1" class="table table-striped">
                    <thead>
                      <tr>
                        <th>Nomor</th>
                        <th>Tanggal</th>
                        <th>Dari</th>
                        <th>Penerima</th>
                        <th>Disposisi</th>
                        <th>Catatan</th>
                      </tr>
                    </thead>
                    <tbody>     
                      <?php
                      $no=1;
                      foreach ($data->result() as $row){ ?> 
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $row->tanggal; ?></td>
                        <td><?php echo $row->nama_pegawai; ?></td>
                        <td><?php echo $row->penerima; ?></td>
                        <td><?php echo $row->disposisi; ?></td>
                        <td><?php echo $row->pesan_singkat; ?></td>
                      </tr>
                      <?php 
                      $no++;} ?>
                    </tbody>
                  </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
      <div class="col-xs-12">
        
        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
          Lembar ini hanya untuk internal Bank Indonesia.
        </p>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>