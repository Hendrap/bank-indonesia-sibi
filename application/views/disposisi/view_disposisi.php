                  <div class="box box-default">
                    <div class="box-header with-border">
                      <h3 class="box-title">Informasi</h3>
                      <div class="box-tools pull-right">      
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                      </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body">

                      <table>
                      <?php foreach ($ddm->result() as $row) : ?>
                      <tr><td>Nomor Dokumen   </td><td>:<strong><?php echo $row->nomor_dokumen; ?></strong></td><tr>
                      <tr><td>Tanggal Masuk   </td><td>:<?php echo $row->tanggal_masuk; ?></td><tr>
                      <tr><td>Tanggal Dokumen   </td><td>:<?php echo $row->tanggal_dokumen; ?></td><tr>
                      <tr><td>Perihal   </td><td>:<?php echo $row->perihal; ?></td><tr>
                    <?php endforeach; ?>
                    
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nomor</th>
                        <th>Tanggal</th>
                        <th>Dari</th>
                        <th>Penerima</th>
                        <th>Disposisi</th>
                        <th>Catatan</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>     
                      <?php
                      $no=1;
                      foreach ($data->result() as $row){ ?> 
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $row->tanggal; ?></td>
                        <td><?php echo $row->nama_pegawai; ?></td>
                        <td><a href="<?php echo base_url(); ?>index.php/penerima_disposisi/index/<?php echo $row->kode_disposisi; ?>"><?php echo $row->penerima; ?></a></td>
                        <td><?php echo $row->disposisi; ?></td>
                        <td><?php echo $row->pesan_singkat; ?></td>
                        <td>
                          <?php if($this->session->userdata('jabatan') == 'administrator'){ ?>
                            <a href="<?php echo base_url(); ?>index.php/disposisi/delete/<?php echo $row->kode_disposisi; ?>" class="btn btn-default"><i class="fa fa-trash"></i></a>
                          <?php } ?>
                        </td>
                      </tr>
                      <?php 
                      $no++;} ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nomor</th>
                        <th>Tanggal</th>
                        <th>Dari</th>
                        <th>Penerima</th>
                        <th>Disposisi</th>
                        <th>Catatan</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
                  <div class="margin">
                    <a href="<?php echo base_url();?>index.php/disposisi/cetak/<?php echo $row->kode_ddm; ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                  </div>