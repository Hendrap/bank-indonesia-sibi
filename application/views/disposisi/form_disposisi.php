          
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Buat Baru</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?php echo base_url();?>index.php/disposisi/simpan"> 
                  <div class="box-body">
                    <div class="form-group">
                      <input type="hidden" class="form-control" name="kode_disposisi" id="exampleInputText1" value="<?php echo $kode_disposisi; ?>">
                      <input type="hidden" class="form-control" name="kode_ddm" id="exampleInputText1" value="<?php echo $kode_ddm; ?>">
                    </div>
                    <div class="form-group">
                        <?php if($this->session->userdata('jabatan')!='pegawai' && $show_lembar=='1'){ ?>
                        <label>Lembar Disposisi yang digunakan :</label>
                        <select name="lembar" class="form-control" value="">
                          <option value="internal">Internal</option>
                          <option value="pusat">Pusat</option>
                        </select>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                      <label>Tanggal</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                    <div class="col-xs-10">
                      <input type="text" name="tanggal" class="form-control"  value="<?php echo $tanggal;?>" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask/>
                    </div>
                      <button type="button" class="btn btn-primary" onclick="today()">today</button>
                      </div><!-- /.input group -->
                    </div><!-- /.form group -->
                  <div class="form-group">
                        <label>Dari</label>
                        <?php if($this->session->userdata('jabatan')=='administrator'){ ?>
                        <select name="dari" class="form-control select2" style="width: 100%;" value="<?php echo $old_dari; ?>">
                          <option value="<?php echo $old_dari; ?>"></option>
                          <?php
                          foreach ($dari->result() as $row){
                          ?>
                          <option value="<?php echo $row->nip; ?>"> <?php echo $row->nama_pegawai; ?></option>
                          <?php } ?>
                        </select>
                        <?php } else { ?>
                        <input type="hidden" class="form-control" name="dari" id="exampleInputText1" value="<?php echo $this->session->userdata('nip'); ?>">
                        <input type="text" class="form-control" id="exampleInputText1" value="<?php echo $this->session->userdata('nip'); ?>" disabled>
                        <?php } ?>
                  </div>
                  <div class="form-group">
                    <label>Penerima disposisi</label>
                    <select name="penerima[]" class="form-control select2" multiple="multiple" placeholder="pilih pegawai" style="width: 100%;" value="<?php echo $old_penerima; ?>">
                          <?php
                          foreach ($penerima->result() as $row){
                          ?>
                          <option value="<?php echo $row->nip; ?>"> <?php echo $row->nama_pegawai; ?></option>
                          <?php } ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Disposisi</label>
                    <select name="disposisi" class="form-control select2" style="width: 100%;" value="<?php echo $disposisi; ?>">
                      <option value="<?php echo $disposisi; ?>"> </option>
                      <option value="Setuju">Setuju</option>
                      <option value="Tolak">Tolak</option>
                      <option value="Untuk diteliti dan pendapat">Untuk diteliti dan pendapat</option>
                      <option value="Untuk diketahui">Untuk diketahui</option>
                      <option value="Untuk diselenggarakan sesuai ketentuan yang berlaku">Untuk diselenggarakan sesuai ketentuan yang berlaku</option>
                      <option value="Sesuai catatan">Sesuai catatan</option>
                      <option value="Untuk perhatian">Untuk perhatian</option>
                      <option value="Untuk diedarkan">Untuk diedarkan</option>
                      <option value="Untuk dijawab">Untuk dijawab</option>
                      <option value="Untuk diperbaiki">Untuk diperbaiki</option>
                      <option value="Untuk dibicarakan dengan saya">Untuk dibicarakan dengan saya</option>
                      <option value="Untuk dibicarakan bersama">Untuk dibicarakan bersama</option>
                      <option value="Untuk diingatkan">Untuk diingatkan</option>
                      <option value="Untuk disimpan">Untuk disimpan</option>
                      <option value="Untuk disiapkan">Untuk disiapkan</option>
                      <option value="Untuk dijadwalkan">Untuk dijadwalkan</option>
                      <option value="Untuk dihadiri/diwakili">Untuk dihadiri/diwakili</option>
                      <option value="Dokumen asli kepada">Dokumen asli kepada</option>
                    </select>
                  </div>
                  <div class="form-group">
                      <label>Pesan Singkat</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..." name="pesan_singkat"><?php echo $pesan_singkat;?></textarea>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
                <script type="text/javascript">

                function today(){var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth()+1; //January is 0!
                    var yyyy = today.getFullYear();

                    if(dd<10) {
                        dd='0'+dd
                    } 

                    if(mm<10) {
                        mm='0'+mm
                    } 

                    today = yyyy+'/'+mm+'/'+dd;
                    document.getElementsByName('tanggal')[0].value = today;
                }
                </script>