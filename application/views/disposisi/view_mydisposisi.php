
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nomor</th>
                        <th>Tanggal</th>
                        <th>Nomor</th>
                        <th>Dari</th>
                        <th>Disposisi</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>     
                      <?php
                      $no=1;
                      foreach ($data->result() as $row){ ?> 
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $row->tanggal; ?></td>
                        <td><?php echo $row->nomor_dokumen; ?></td>
                        <td><?php echo $row->nama_pegawai; ?></td>
                        <td><?php echo $row->disposisi; ?></td>
                        <td>
                        <?php if( ($row->cek)=='0'){
                          ?>
                            <i class="fa fa-warning"></i> <span>Belum Diterima</span>
                          <?php 
                          }else{
                          ?>
                            <i class="fa fa-check"></i> <span>Diterima</span>
                          <?php } ?>
                        </td>
                        <td>
                        <?php if( ($row->cek)=='0'){
                          ?>
                            <a href="<?php echo base_url(); ?>index.php/penerima_disposisi/state/<?php echo $row->kode_penerima; ?>"><span class="label label-warning">Terima</span></a>
                          <?php 
                          }else{
                          ?>
                            <a href="<?php echo base_url(); ?>index.php/disposisi/index/<?php echo $row->kode_ddm; ?>"><span class="label label-success">Forward</span></a>
                          <?php } ?>
                        </td>
                      </tr>
                      <?php 
                      $no++;} ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nomor</th>
                        <th>Tanggal</th>
                        <th>Nomor</th>
                        <th>Dari</th>
                        <th>Disposisi</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>