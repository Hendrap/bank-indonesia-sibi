
<section class="content">
          <div class="row">
            <div class="col-md-6">

              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Entri Data Dokumen Masuk</h3>
                </div>
                <form role="form" name="form1" method="post" action="<?php echo base_url();?>index.php/ddm/simpan">
                <div class="box-body"> 

                    <div class="form-group">
                      <input type="hidden" name="kode_ddm" class="form-control" id="exampleInputText1" value="<?php echo $kode_ddm; ?>"/>
                      <input type="hidden" name="temp" class="form-control" id="temp" value=""/>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Nomor Dokumen</label>
                      <input type="text" name="nomor_dokumen" class="form-control" id="exampleInputText1" placeholder="__/__/_____/___/__" value="<?php echo $nomor_dokumen;?>" onblur="check_ava()">
                      <div id='username_availability_result'></div>
                    </div>

                    <div class="form-group">
                      <label>Tanggal Dokumen</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                          <div class="col-xs-10">
                            <input type="text" name="tanggal_dokumen" class="form-control"  value="<?php echo $tanggal_dokumen;?>" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask/>
                          </div>
                            <button type="button" class="btn btn-primary" onclick="setDocToday()">today</button>
                      </div><!-- /.input group -->
                    </div><!-- /.form group -->
                    <div class="form-group">
                      <label>Tanggal Masuk</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                          <div class="col-xs-10">
                            <input type="text" name="tanggal_masuk" class="form-control"  value="<?php echo $tanggal_masuk;?>" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask/>
                          </div>
                            <button type="button" class="btn btn-primary" onclick="setInToday()">today</button>
                        </div><!-- /.input group -->
                    </div><!-- /.form group -->
                    <div class="form-group">
                      <label>Perihal</label>
                      <textarea class="form-control" rows="3" placeholder="" name="perihal"><?php echo $perihal;?></textarea>
                    </div>
                    <div class="form-group">
                      <label>Ditujukan Kepada <small>(isikan sebagai penjelas tujuan kepada orang tertentu jika perlu)</small></label>
                      <textarea class="form-control" rows="3" placeholder="" name="ditujukan_kepada"><?php echo $ditujukan_kepada;?></textarea>
                    </div>
                    <div class="form-group">
                      <label>Keterangan</label>
                      <textarea class="form-control" rows="3" placeholder="" name="keterangan"><?php echo $keterangan;?></textarea>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit"  class="btn btn-primary" onclick="return valid()">Simpan</button>
                </div>
                </form>
              </div><!-- /.box -->


            </div><!-- /.col (left) -->

            <div class="col-md-6">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Informasi</h3>
                </div>
                <div class="box-body">
                  <!-- Date range -->
                  <p> Format nomor dokumen : </br>
                    <strong>tahun/nomor/pemrakarsa/jenis_dokumen/sifat</strong> </br>
                    Contoh : </br>17/1/DPSI-GKSI-SKTI/M.02/B

                  </p>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->

        <script type="text/javascript">
          function setDocToday(){
            document.getElementsByName('tanggal_dokumen')[0].value = today();
          }
          function setInToday(){
            document.getElementsByName('tanggal_masuk')[0].value = today();
          }
          function today(){
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();
            if(dd<10) {
              dd='0'+dd
            } 
            if(mm<10) {
              mm='0'+mm
            } 
            today = yyyy+'/'+mm+'/'+dd;
            return today;
          }
          function valid(){
            if(form1.temp.value==0){
              alert("data harus terisi dengan benar");
              return false;
            }
          }
          function check_ava(){
            var temp = document.getElementsByName('nomor_dokumen')[0].value;
            jQuery.ajax({
                              type: "POST",
                              url: "<?php echo base_url(); ?>" + "index.php/ddm/checkId",
                              dataType: 'json',
                              data: {id: temp},
                              success: function(res) {
                                  if (res.username == 1)
                                  {
                                    form1.temp.value = 1;
                                    jQuery("div#username_availability_result").html('<span class="is_available">' +temp + ' is Available</span>');
                                  }
                                  else{
                                    form1.temp.value = 0;
                                    jQuery("div#username_availability_result").html('<span class="is_not_available">' +temp + ' is not Available</span>');
                                  }
                              }
                          });
          }
      </script>
      <style type='text/css'>
      .is_available{
        color:blue;
      }
      .is_not_available{
        color:red;
      }
      </style>
        </script>
