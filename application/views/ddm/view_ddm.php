
                <div align="center">
                <form method="post" action="<?php echo base_url(); ?>/index.php/ddm/filter?departemen=<?php echo $old_dept; ?>&grup=<?php echo $old_grup; ?>&divisi=<?php echo $old_divisi; ?>">
                        <i class="fa fa-filter"></i> Show filter : </br>
                        Departemen
                        <select name="departemen">
                          <option value="">All</option>
                          <?php
                          foreach ($bydepartemen->result() as $row){
                          ?>
                          <option value="<?php echo $row->kode_departemen; ?>"> <?php echo $row->kode_departemen; ?></option>
                          <?php } ?>
                          <option value="<?php echo $my_dept; ?>" selected><?php echo $my_dept; ?></option>
                        </select>
                        Jenis Dokumen
                        <select name="jenis">
                          <option value="">All</option>
                          <?php
                          foreach ($byjenisdok->result() as $row){
                          ?>
                          <option value="<?php echo $row->kode_jenisdok; ?>"> <?php echo $row->kode_jenisdok; ?></option>
                          <?php } ?>
                          <option value="<?php echo $old_jenis; ?>" selected><?php echo $old_jenis; ?></option>
                        </select>
                        Tahun
                        <select name="tahun">
                          <option value="">All</option>
                          <?php
                          foreach ($bytahun->result() as $row){
                          ?>
                          <option value="<?php echo $row->kode_tahun; ?>"> <?php echo $row->kode_tahun; ?></option>
                          <?php } ?>
                          <option value="<?php echo $old_tahun; ?>" selected><?php echo $old_tahun; ?></option>
                        </select>
                        
                        <button type="submit"  class="btn">Filter</button>
                </form>
              </div>


                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nomor</th>
                        <th>Nomor Dokumen Masuk</th>
                        <th>Tanggal Dokumen</th>
                        <th>Tanggal Masuk</th>
                        <th>Perihal</th>
                        <th>Ditujukan Kepada</th>
                        <th>Posisi</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>     
                      <?php
                      $no=1;
                      foreach ($data->result() as $row){ ?> 
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $row->nomor_dokumen; ?></td>
                        <td><?php echo $row->tanggal_dokumen; ?></td>
                        <td><?php echo $row->tanggal_masuk; ?></td>
                        <td><?php echo $row->perihal; ?></td>
                        <td><?php echo $row->ditujukan_kepada; ?></td>
                        <td><?php 
                        $q=$this->db->query("SELECT penerima from disposisi WHERE kode_disposisi = (SELECT MAX(kode_disposisi) FROM disposisi where kode_ddm = '$row->kode_ddm')");
                        if($q->num_rows()>0){
                          $penerima=$q->row();
                          echo $penerima->penerima;
                        }
                         ?></td>
                        <td><?php echo $row->keterangan; ?></td>
                        <?php if($this->session->userdata('jabatan')=='pegawai'){ ?>
                        <td>
                          <a href="<?php echo base_url(); ?>index.php/disposisi/index/<?php echo $row->kode_ddm; ?>" class="btn btn-default"><i class="fa fa-folder-open-o"></i></a>
                        </td>
                      <?php }
                      else{ ?>
                        <td>
                          <a href="<?php echo base_url(); ?>index.php/disposisi/index/<?php echo $row->kode_ddm; ?>" class="btn btn-default"><i class="fa fa-folder-open-o"></i></a>
                          <a href="<?php echo base_url(); ?>index.php/ddm/edit/<?php echo $row->kode_ddm; ?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                          <a href="<?php echo base_url(); ?>index.php/ddm/delete/<?php echo $row->kode_ddm; ?>" class="btn btn-default"><i class="fa fa-trash"></i></a>
                        </td>
                      <?php } ?>
                      </tr>
                      <?php 
                      $no++;} ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nomor</th>
                        <th>Nomor Dokumen Masuk</th>
                        <th>Tanggal Dokumen</th>
                        <th>Tanggal Masuk</th>
                        <th>Perihal</th>
                        <th>Ditujukan Kepada</th>
                        <th>Posisi</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
                  <div class="margin">
                    <a href="<?php echo base_url();?>index.php/ddm/cetak?jenis=<?php echo $old_jenis; ?>&tahun=<?php echo $old_tahun; ?>&departemen=<?php echo $old_dept; ?>&grup=<?php echo $old_grup; ?>&divisi=<?php echo $old_divisi; ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                  </div>