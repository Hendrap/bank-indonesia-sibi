<table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nomor</th>
                        <th>Nomor Dokumen Masuk</th>
                        <th>Tanggal Dokumen</th>
                        <th>Tanggal Masuk</th>
                        <th>Perihal</th>
                      </tr>
                    </thead>
                    <tbody>     
                      <?php
					  $data = "SELECT * FROM ddm WHERE kode_jenisdok = {$kode_jenisdok}";
                      $no=1;
                      foreach ($data->result() as $row){ ?> 
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $row->nomor_dokumen; ?></td>
                        <td><?php echo $row->tanggal_dokumen; ?></td>
                        <td><?php echo $row->tanggal_masuk; ?></td>
                        <td><?php echo $row->perihal; ?></td>
                      </tr>
                      <?php 
                      $no++;} ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nomor</th>
                        <th>Nomor Dokumen Masuk</th>
                        <th>Tanggal Dokumen</th>
                        <th>Tanggal Masuk</th>
                        <th>Perihal</th>
                      </tr>
                    </tfoot>
                  </table>