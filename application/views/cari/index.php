<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Input description</title>
<script src="<?php echo base_url(); ?>/assets/plugins/jQuery/jquery.js"></script>
<script type="text/javascript">


  $(document).ready(function() {
        
		
		//the min chars for username
		var min_chars = 3;
		
		//result texts
		var characters_error = 'Minimum amount of chars is 3';
		var checking_html = '<img src="<?php echo base_url(); ?>assets/dist/img/loading.gif" /> Checking...';
		
		//when button is clicked
		$('#check_username_availability').click(function(){
			//run the character number check
			if($('#username').val().length < min_chars){
				//if it's bellow the minimum show characters_error text
				$('#username_availability_result').html(characters_error);
			}else{			
				//else show the cheking_text and run the function to check
				$('#username_availability_result').html(checking_html);
				check_availability();
			}
		});
		
		
  });

//function to check username availability	
function check_availability(){
		
		//get the username
		var username = $('#username').val();
		
		//use ajax to run the check
		$.post("check_username.php", { username: username },
			function(result){
				//if the result is 1
				if(result == "oke"){
					//show that the username is available
					$('#username_availability_result').html('<span class="is_available"><b>' +result+ '</b> is Available</span>');
				}else{
					//show that the username is NOT available
					$('#username_availability_result').html('<span class="is_not_available">' +result + ' is not Available</span>');
			}
		});

}  
</script>
<style type='text/css'>
#check_username_availability{
	background: #225384;
	border:1px solid black;
	color:white;
}
body{
	font-family: 'tahoma';
}
input{
	padding:5px;
	font-family: 'tahoma';
}
.is_available{
	color:blue;
}
.is_not_available{
	color:red;
}
</style>
</head>
<body>
<input type='text' id='username'> <input type='button' id='check_username_availability' value='Check'>
<div id='username_availability_result'></div>
</body>
</html>
