          
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Edit Tahun Buku</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?php echo base_url();?>index.php/tahun_buku/simpan"> 
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputText1">Kode Tahun : <?php echo $kode_tahun; ?></label>
                      <input type="hidden" class="form-control" name="kode_tahun" id="exampleInputText1" placeholder="Enter Kode" value="<?php echo $kode_tahun; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Tahun Buku</label>
                      <input type="text" class="form-control" name="tahun_buku" id="exampleInputText2" placeholder="Tahun Buku" value="<?php echo $tahun_buku; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Tahun Dokumen</label>
                      <input type="text" class="form-control" name="tahun_dokumen" id="exampleInputText2" placeholder="Tahun Dokumen" value="<?php echo $tahun_dokumen; ?>">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                </form>