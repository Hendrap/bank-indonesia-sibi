
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nomor</th>
                        <th>Kode</th>
                        <th>Tahun buku</th>
                        <th>Tahun Dokumen</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>     
                      <?php
                      $no=1;
                      foreach ($data->result() as $row){ ?> 
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $row->kode_tahun; ?></td>
                        <td><?php echo $row->tahun_buku; ?></td>
                        <td><?php echo $row->tahun_dokumen; ?></td>
                        <td>
                          <a href="<?php echo base_url(); ?>index.php/tahun_buku/edit/<?php echo $row->kode_tahun; ?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                          <a href="<?php echo base_url(); ?>index.php/tahun_buku/delete/<?php echo $row->kode_tahun; ?>" class="btn btn-default"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                      <?php 
                      $no++;} ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nomor</th>
                        <th>Kode</th>
                        <th>Tahun buku</th>
                        <th>Tahun Dokumen</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>