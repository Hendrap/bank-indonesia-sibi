<aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url(); ?>database/uploads/<?php echo $this->session->userdata('thumb'); ?>" class="img-circle" alt="No Image" />
            </div>
            <div class="pull-left info">
              <p><?php echo $this->session->userdata('nama_pegawai');?></p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <?php
          $level = $this->session->userdata('jabatan');
          if($level =='pegawai'){
          ?>

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">Main Navigation</li>
            <!-- Optionally, you can add icons to the links -->
            <li>
              <a href="<?php echo base_url();?>index.php/home">
                <i class="fa fa-home"></i> <span>Beranda</span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>index.php/pegawai/myprofile">
                <i class="fa fa-user"></i> <span>Profil saya</span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>index.php/disposisi/mydisposisi">
                <i class="fa fa-mail-forward"></i> <span>Disposisi saya</span> <small class="label pull-right bg-green">new</small>
              </a>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-inbox"></i><span>Dokumen masuk</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url();?>index.php/ddm"><i class="fa fa-folder-open"></i></i> <span>Lihat Tabel</span></a></li>
                <li><a href="<?php echo base_url();?>index.php/ddm"><i class="fa fa-folder-open"></i></i> <span>Filter</span><i class="fa fa-angle-left pull-right"></i></a>
                  <!-- departemen -->
                  <ul class="treeview-menu">
                    <?php 
                    $sql=$this->db->query("SELECT * from departemen");
                    foreach ($sql->result() as $row) { ?>
                      <li><a href="<?php echo base_url();?>index.php/ddm/bysatker?departemen=<?php echo $row->kode_departemen; ?>&grup=&divisi="><i class="fa fa-circle-o"></i> <?php echo $row->kode_departemen;?><i class="fa fa-angle-left pull-right"></i></a>
                        <!-- grup -->
                        <?php 
                        $sql=$this->db->query("SELECT * from grup WHERE kode_departemen= '$row->kode_departemen'");
                        if($sql->num_rows()>0){ ?>

                        <ul class="treeview-menu">
                          <?php
                          foreach ($sql->result() as $row) { ?>
                            <li><a href="<?php echo base_url();?>index.php/ddm/bysatker?departemen=<?php echo $row->kode_departemen; ?>&grup=<?php echo $row->kode_grup; ?>&divisi="><i class="fa fa-circle-o"></i><?php echo $row->kode_grup;?><i class="fa fa-angle-left pull-right"></i></a>
                                <!-- grup -->
                                <?php 
                                  $sql=$this->db->query("SELECT * from divisi WHERE kode_grup = '$row->kode_grup'");
                                  if($sql->num_rows()>0){ ?>

                                <ul class="treeview-menu">
                                  <?php 
                                  foreach ($sql->result() as $row) { ?>
                                    <li><a href="<?php echo base_url();?>index.php/ddm/bysatker?departemen=<?php echo $row->kode_departemen; ?>&grup=<?php echo $row->kode_grup; ?>&divisi=<?php echo $row->kode_divisi;?>"><i class="fa fa-circle-o"></i><?php echo $row->kode_divisi;?></a></li>
                                  <?php }?>
                                </ul>
                                <?php } ?>
                            </li>
                          <?php }?>
                        </ul>
                        <?php } ?>
                      </li>
                    <?php }?>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-upload"></i><span>Dokumen keluar</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url();?>index.php/ddk"><i class="fa fa-folder-open"></i></i> <span>Lihat Tabel</span></a></li>
              </ul>
            </li>
          </ul><!-- /.sidebar-menu -->
          <?php 
          }else{
          ?>

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">Main Navigation</li>
            <!-- Optionally, you can add icons to the links -->
            <li>
              <a href="<?php echo base_url();?>index.php/home">
                <i class="fa fa-home"></i> <span>Beranda</span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url();?>index.php/pegawai/myprofile">
                <i class="fa fa-user"></i> <span>Profil saya</span>
              </a>
            </li>
            <?php if($level=='sekretaris'){ ?>
            <li>
              <a href="<?php echo base_url();?>index.php/disposisi/mydisposisi">
                <i class="fa fa-mail-forward"></i> <span>Disposisi saya</span> <small class="label pull-right bg-green">new</small>
              </a>
            </li>
            <?php } ?>
            <li class="treeview">
              <a href="#"><i class="fa fa-inbox"></i><span>Dokumen masuk</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url();?>index.php/ddm/tambah"><i class="fa fa-edit"></i> <span>Buat Baru</span></a></li>
                <li><a href="<?php echo base_url();?>index.php/ddm"><i class="fa fa-folder-open"></i></i> <span>Lihat Tabel</span></a></li>
                <li><a href="<?php echo base_url();?>index.php/ddm"><i class="fa fa-folder-open"></i></i> <span>Filter</span><i class="fa fa-angle-left pull-right"></i></a>
                  <!-- departemen -->
                  <ul class="treeview-menu">
                    <?php 
                    $sql=$this->db->query("SELECT * from departemen");
                    foreach ($sql->result() as $row) { ?>
                      <li><a href="<?php echo base_url();?>index.php/ddm/bysatker?departemen=<?php echo $row->kode_departemen; ?>&grup=&divisi="><i class="fa fa-circle-o"></i> <?php echo $row->kode_departemen;?><i class="fa fa-angle-left pull-right"></i></a>
                        <!-- grup -->
                        <?php 
                        $sql=$this->db->query("SELECT * from grup WHERE kode_departemen= '$row->kode_departemen'");
                        if($sql->num_rows()>0){ ?>

                        <ul class="treeview-menu">
                          <?php
                          foreach ($sql->result() as $row) { ?>
                            <li><a href="<?php echo base_url();?>index.php/ddm/bysatker?departemen=<?php echo $row->kode_departemen; ?>&grup=<?php echo $row->kode_grup; ?>&divisi="><i class="fa fa-circle-o"></i><?php echo $row->kode_grup;?><i class="fa fa-angle-left pull-right"></i></a>
                                <!-- grup -->
                                <?php 
                                  $sql=$this->db->query("SELECT * from divisi WHERE kode_grup = '$row->kode_grup'");
                                  if($sql->num_rows()>0){ ?>

                                <ul class="treeview-menu">
                                  <?php 
                                  foreach ($sql->result() as $row) { ?>
                                    <li><a href="<?php echo base_url();?>index.php/ddm/bysatker?departemen=<?php echo $row->kode_departemen; ?>&grup=<?php echo $row->kode_grup; ?>&divisi=<?php echo $row->kode_divisi;?>"><i class="fa fa-circle-o"></i><?php echo $row->kode_divisi;?></a></li>
                                  <?php }?>
                                </ul>
                                <?php } ?>
                            </li>
                          <?php }?>
                        </ul>
                        <?php } ?>
                      </li>
                    <?php }?>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-upload"></i><span>Dokumen keluar</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url();?>index.php/ddk/tambah"><i class="fa fa-edit"></i> <span>Buat Baru</span></a></li>
                <li><a href="<?php echo base_url();?>index.php/ddk"><i class="fa fa-folder-open"></i></i> <span>Lihat Tabel</span></a></li>
              </ul>
            </li>
            <?php if($this->session->userdata('jabatan')=='administrator'){ ?>
            <li class="treeview">
              <a href="#"><i class="fa fa-book"></i><span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url();?>index.php/tahun_buku">Tahun buku</a></li>
                <li><a href="<?php echo base_url();?>index.php/departemen">Departemen</a></li>
                <li><a href="<?php echo base_url();?>index.php/grup">Grup</a></li>
                <li><a href="<?php echo base_url();?>index.php/divisi">Divisi</a></li>
                <li><a href="<?php echo base_url();?>index.php/jenis_dokumen">Jenis dokumen</a></li>
                <li><a href="<?php echo base_url();?>index.php/pegawai">Pegawai</a></li>
              </ul>
            </li>
            <?php } ?>
            
          </ul><!-- /.sidebar-menu -->
          <?php }
          ?>
        </section>
        <!-- /.sidebar -->
      </aside>