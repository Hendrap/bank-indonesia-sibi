<script type="text/javascript">
  function goAJax() {
    jQuery.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>" + "index.php/disposisi/notifme",
      dataType: 'json',
        success: function(res) {
        }
      });
      }
  window.onload = goAjax;
</script> 

<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SI</b>BI</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Documentation </b>BI</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url(); ?>database/uploads/<?php echo $this->session->userdata('thumb'); ?>" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?php echo $this->session->userdata('nama_pegawai');?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo base_url(); ?>database/uploads/<?php echo $this->session->userdata('foto'); ?>" class="img-circle" alt="User Image" />
                    <p>
                      <?php echo $this->session->userdata('nama_pegawai');?> - <?php echo $this->session->userdata('kode_divisi');?>
                      <small><?php echo $this->session->userdata('jabatan');?></small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url();?>index.php/pegawai/myprofile" class="btn btn-default btn-flat">Profil</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url();?>/index.php/home/logout/" class="btn btn-default btn-flat">Sign Out</a>
                    </div>
                  </li>
                </ul>
              </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>