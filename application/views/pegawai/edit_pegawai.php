          
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Edit Pegawai</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php echo  form_open_multipart('pegawai/uploadImage')?> 
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputText1">NIP : <?php echo $nip; ?></label>
                      <input type="hidden" class="form-control" name="nip" id="exampleInputText1" placeholder="Enter nip" value="<?php echo $nip; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Nama Pegawai</label>
                      <input type="text" class="form-control" name="nama_pegawai" id="exampleInputText1" placeholder="Nama" value="<?php echo $nama_pegawai; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Username</label>
                        <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>">
                        <div id='username_availability_result'></div>
                        <input type='button' id='checkId' value='cek' class='btn btn-primary' onclick="check_ava()">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputText1">Password <small> (kosongkan jika tidak ada perubahan) </small></label>
                      <input type="password" class="form-control" name="password" id="exampleInputText1" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label>Level user</label>
                        <select name="jabatan" class="form-control" value="<?php echo $jabatan; ?>">
                          <option value="<?php echo $jabatan; ?>" selected><?php echo $jabatan; ?></option>
                          <option value="administrator">administrator</option>
                          <option value="sekretaris">sekretaris</option>
                          <option value="pegawai">pegawai</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Divisi</label>
                        <select name="kode_divisi" class="form-control select2" style="width: 100%;" value="<?php echo $old_kode_divisi; ?>">
                          <option value="<?php echo $old_kode_divisi; ?>"><?php echo $old_kode_divisi; ?></option>
                          <?php
                          foreach ($kode_divisi->result() as $row){
                          ?>
                          <option value="<?php echo $row->kode_divisi; ?>"> <?php echo $row->nama_divisi;?> (<?php echo $row->kode_divisi; ?>) </option>
                          <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Status Aktif <small> (Kosongkan jika tidak ada perubahan)</label>
                        <select name="aktif" class="form-control" value="<?php echo $aktif; ?>">
                          <option value="<?php echo $aktif; ?>"></option>
                          <option value="1">Aktif</option>
                          <option value="0">Non Aktif</option>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Upload Foto</label>
                      <input type="file" name="userfile">
                      <input type="hidden" class="form-control" name="old_foto" value="<?php echo $temp_foto; ?>">
                      <p class="help-block">Maksimal 50 karakter (jpg,jpeg,png) , max ukuran 5MB (1900 x 1280 px)</p>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                <?php echo form_close();?>

<script type="text/javascript">
    function check_ava(){
      var user_name = document.getElementsByName('username')[0].value;
      jQuery.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>" + "index.php/pegawai/checkId",
                        dataType: 'json',
                        data: {username: user_name},
                        success: function(res) {
                            if (res.username == 1)
                            {
                              jQuery("div#username_availability_result").html('<span class="is_available">' +user_name + ' is Available</span>');
                            }
                            else{
                              jQuery("div#username_availability_result").html('<span class="is_not_available">' +user_name + ' is not Available</span>');
                            }
                        }
                    });
    }
</script>
<style type='text/css'>
.is_available{
  color:blue;
}
.is_not_available{
  color:red;
}
</style>