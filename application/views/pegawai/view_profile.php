    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive user-image" src="<?php echo base_url(); ?>database/uploads/<?php echo $foto; ?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $nama_pegawai; ?></h3>

              <p class="text-muted text-center"><?php echo $kode_divisi; ?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Username</b> <a class="pull-right"><?php echo $username; ?></a>
                </li>
                <li class="list-group-item">
                  <b>NIP</b> <a class="pull-right"><?php echo $nip; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Status</b> <a class="pull-right">Online</a>
                </li>
              </ul>

              <a href="<?php echo base_url();?>index.php/pegawai/editmyprofile/" class="btn btn-primary btn-block"><b>Edit</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Tugas saya</a></li>
              <li><a href="#timeline" data-toggle="tab">Aktivitas saya</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                
                <?php 
                foreach ($disposisi->result() as $row) {
                ?>
                <!-- Post -->
                <div class="post">
                  <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="<?php echo base_url(); ?>database/uploads/<?php echo $row->foto; ?>" alt="user image">
                        <span class="username">
                          <a href="<?php echo base_url();?>index.php/disposisi/mydisposisi"><?php echo $row->nama_pegawai; ?></a>
                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                    <span class="description"><?php echo $row->nomor_dokumen; ?> - <?php $row->tanggal; ?></span>
                  </div>
                  <!-- /.user-block -->
                    <p>
                     <?php echo $row->disposisi; ?> : " <?php echo $row->perihal; ?> "
                    </p>
                  <ul class="list-inline">
                    <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Catatan anda :</a></li>
                    <li class="pull-right">
                      <a href="<?php echo base_url();?>index.php/disposisi/mydisposisi" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Lihat detail</a></li>
                  </ul>

                  <input class="form-control input-sm" type="text" placeholder="Type a comment" value="<?php echo $row->pesan_singkat; ?>">
                </div>
                <!-- /.post -->
                <?php } ?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
                <!-- The timeline -->
                <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-red">
                          All time
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <?php 
                  foreach ($activity->result() as $row) {
                  ?>
                  <li>
                    <i class="fa fa-envelope bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> <?php echo $row->tanggal; ?></span>

                      <h3 class="timeline-header">anda memberikan disposisi kepada <a href="#"><?php echo $row->nama_pegawai; ?></a></h3>

                      <div class="timeline-body"><?php echo $row->nomor_dokumen; ?>
                      </div>
                      <div class="timeline-footer">
                        <a class="btn btn-primary btn-xs" href="<?php echo base_url(); ?>index.php/disposisi/index/<?php echo $row->kode_ddm; ?>">Lihat</a>
                        <?php if($row->cek=='0'){
                          echo "<a class='btn btn-danger btn-xs'>Belum diterima</a> ";
                        }else{
                          echo "<a class='btn btn-success btn-xs'>Diterima</a> ";
                        } ?>
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <?php } ?>
                  <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                  </li>
                </ul>
              </div>
              <!-- /.tab-pane -->

              
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
