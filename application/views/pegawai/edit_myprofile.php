          
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Buat Baru</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php echo  form_open_multipart('pegawai/uploadImage')?> 
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputText1">NIP</label>
                      <input type="hidden" class="form-control" name="nip" id="exampleInputText1" placeholder="Enter nip" value="<?php echo $nip; ?>">
                      <input type="text" class="form-control" value="<?php echo $nip; ?>" disabled>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Nama Pegawai</label>
                      <input type="text" class="form-control" name="nama_pegawai" id="exampleInputText1" placeholder="Nama" value="<?php echo $nama_pegawai; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Username</label>
                      <input type="hidden" class="form-control" name="username" id="exampleInputText1" placeholder="Username" value="<?php echo $username; ?>">
                      <input type="text" class="form-control" value="<?php echo $username; ?>" disabled>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Password <small> (kosongkan jika tidak ada perubahan) </small></label>
                      <input type="text" class="form-control" name="password" id="exampleInputText1" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <input type="hidden" class="form-control" name="jabatan" value="<?php echo $jabatan; ?>">
                      <input type="hidden" class="form-control" name="kode_divisi" value="<?php echo $old_kode_divisi; ?>">
                      <input type="hidden" class="form-control" name="aktif" value="<?php echo $aktif; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Upload Foto</label>
                      <input type="file" name="userfile">
                      <input type="hidden" class="form-control" name="old_foto" value="<?php echo $temp_foto; ?>">
                      <p class="help-block">Maksimal 50 karakter (jpg,jpeg,png)</p>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                <?php echo form_close();?>