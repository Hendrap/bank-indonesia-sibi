
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nomor</th>
                        <th>NIP</th>
                        <th>Nama Pegawai</th>
                        <th>Divisi</th>
                        <th> Status </th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>     
                      <?php
                      $no=1;
                      foreach ($data->result() as $row){ ?> 
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $row->nip; ?></td>
                        <td><?php echo $row->nama_pegawai; ?></td>
                        <td><?php echo $row->kode_divisi; ?></td>
                        <td>
                          <?php if( ($row->aktif)=='1'){
                          ?>
                            <a href="<?php echo base_url(); ?>index.php/pegawai/state/<?php echo $row->nip; ?>/0"><span class="label label-success">Aktif</span></a>
                          <?php 
                          }else{
                          ?>
                              <a href="<?php echo base_url(); ?>index.php/pegawai/state/<?php echo $row->nip; ?>/1"><span class="label label-danger">Non Aktif</span></a>
                          <?php } ?>
                        </td>
                        <td>
                          <a href="<?php echo base_url(); ?>index.php/pegawai/edit/<?php echo $row->nip; ?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                          <a href="<?php echo base_url(); ?>index.php/pegawai/delete/<?php echo $row->nip; ?>" class="btn btn-default"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                      <?php 
                      $no++;} ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nomor</th>
                        <th>NIP</th>
                        <th>Nama Pegawai</th>
                        <th>Divisi</th>
                        <th> Status </th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>