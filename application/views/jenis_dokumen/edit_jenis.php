          
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Edit Jenis Dokumen</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?php echo base_url();?>index.php/jenis_dokumen/simpan"> 
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputText1">Kode Jenis Dokumen : <?php echo $kode_jenisdok; ?></label>
                      <input type="hidden" class="form-control" name="kode_jenisdok" id="exampleInputText1" placeholder="Enter Kode" value="<?php echo $kode_jenisdok; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Jenis Dokumen</label>
                      <input type="text" class="form-control" name="jenis_dokumen" id="exampleInputText2" placeholder="Enter Nama" value="<?php echo $jenis_dokumen; ?>">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                </form>