          
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Edit Departemen</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?php echo base_url();?>index.php/departemen/simpan"> 
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputText1">Kode Departemen : <?php echo $kode_departemen; ?></label>
                      <input type="hidden" class="form-control" name="kode_departemen" id="exampleInputText1" placeholder="Enter Kode" value="<?php echo $kode_departemen; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Nama Departemen</label>
                      <input type="text" class="form-control" name="nama_departemen" id="exampleInputText2" placeholder="Enter Nama" value="<?php echo $nama_departemen; ?>">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                </form>