          
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Buat Baru</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" name="form1" action="<?php echo base_url();?>index.php/departemen/simpan"> 
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputText1">Kode Departemen</label>
                      <input type="text" class="form-control" name="kode_departemen" id="exampleInputText1" placeholder="Enter Kode" value="<?php echo $kode_departemen; ?>" onblur="check_ava()">
                      <div id='username_availability_result'></div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Nama Departemen</label>
                      <input type="text" class="form-control" name="nama_departemen" id="exampleInputText2" placeholder="Enter Nama" value="<?php echo $nama_departemen; ?>">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" onclick="return valid()">Simpan</button>
                  </div>
                </form>
<script type="text/javascript">
    function check_ava(){
      var temp = document.getElementsByName('kode_departemen')[0].value;
      jQuery.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>" + "index.php/departemen/checkId",
                        dataType: 'json',
                        data: {id: temp},
                        success: function(res) {
                            if (res.username == 1)
                            {
                              jQuery("div#username_availability_result").html('<span class="is_available">' +temp + ' is Available</span>');
                            }
                            else{
                              jQuery("div#username_availability_result").html('<span class="is_not_available">' +temp + ' is not Available</span>');
                            }
                        }
                    });
    }
    function valid(){
            if(!form1.kode_departemen.value){
              alert("data harus terisi dengan benar");
              return false;
            }
          }
</script>
<style type='text/css'>
.is_available{
  color:blue;
}
.is_not_available{
  color:red;
}
</style>