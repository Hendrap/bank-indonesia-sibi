
<section class="content">
          <div class="row">
            <div class="col-md-6">

              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Entri Baru</h3>
                </div>
                <form role="form" name="form1" method="post" action="<?php echo base_url();?>index.php/ddk/simpan">
                <div class="box-body"> 
                    <div class="form-group">
                      <label>Tanggal Dokumen</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                          <div class="col-xs-10">
                            <input type="text" name="tanggal_dokumen" class="form-control"  value="<?php echo $tanggal_dokumen;?>" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask/>
                          </div>
                            <button type="button" class="btn btn-primary" onclick="today()">today</button>
                        </div><!-- /.input group -->
                    </div><!-- /.form group -->
                    <div class="form-group">
                        <label>Jenis Dokumen</label>
                        <select name="kode_jenisdok" class="form-control select2" style="width: 100%;" value="<?php echo $old_kode_jenisdok; ?>">
                          <option value="<?php echo $old_kode_jenisdok; ?>"><?php echo $old_kode_jenisdok; ?></option>
                          <?php
                          foreach ($kode_jenisdok->result() as $row){
                          ?>
                          <option value="<?php echo $row->kode_jenisdok; ?>"> <?php echo $row->kode_jenisdok; ?></option>
                          <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Nomor Urut Berdasar Jenis</label>
                      <input type="text" name="no_sesuai_jenis" class="form-control" id="nourut" placeholder="" value="<?php echo $no_sesuai_jenis; ?>" />
                    </div>
                    <div class="form-group">
                      <label>Perihal</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..." name="perihal"><?php echo $perihal;?></textarea>
                    </div>
                    <div class="form-group">
                      <label>Keterangan</label>
                      <textarea class="form-control" rows="3" placeholder="" name="keterangan"><?php echo $keterangan;?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Tahun Dokumen</label>
                        <select name="kode_tahun" class="form-control select2" style="width: 100%;" value="<?php echo $old_kode_tahun; ?>">
                          <option value="<?php echo $old_kode_tahun; ?>"></option>
                          <?php
                          foreach ($kode_tahun->result() as $row){
                          ?>
                          <option value="<?php echo $row->kode_tahun; ?>"> <?php echo $row->tahun_dokumen; ?></option>
                          <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Pemrakarsa<small> (Kosongkan jika tidak perlu) </small></label>
                        <select name="kode_departemen" class="form-control select2" style="width: 100%;" >
                          <option value=""></option>
                          <?php
                          foreach ($kode_departemen->result() as $row){
                          ?>
                          <option value="<?php echo $row->kode_departemen; ?>"><?php echo $row->kode_departemen; ?>  | <?php echo $row->nama_departemen; ?></option>
                          <?php } ?>
                        </select>

                        <select name="kode_grup" class="form-control select2" style="width: 100%;" >
                          <option value=""></option>
                          <?php
                          foreach ($kode_grup->result() as $row){
                          ?>
                          <optgroup label="<?php echo $row->kode_departemen; ?>">
                            <?php $grup = explode(',', $row->grup);
                             foreach ($grup as $kode_grup) { ?>
                              <option value="<?php echo $kode_grup; ?>"> <?php echo $kode_grup; ?>  |  <?php echo $row->nama_grup; ?></option>
                            <?php } ?>
                          </optgroup>
                          <?php } ?>
                        </select>


                        <select name="kode_divisi" class="form-control select2" style="width: 100%;" >
                          <option value=""></option>
                          <?php
                          foreach ($kode_divisi->result() as $row){
                          ?>
                          <optgroup label="<?php echo $row->kode_departemen; ?>">
                            <?php $grup = explode(',', $row->divisi);
                             foreach ($grup as $kode_grup) { ?>
                              <option value="<?php echo $kode_grup; ?>"> <?php echo $kode_grup; ?>  |  <?php echo $row->nama_divisi; ?></option>
                            <?php } ?>
                          </optgroup>
                          <?php } ?>
                        </select>


                        <input type="hidden" name="pemrakarsa" class="form-control" id="exampleInputText1" value="<?php echo $pemrakarsa; ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Ditujukan Kepada <small> (Kosongkan jika tidak perlu) </small></label>
                        <select name="tujuan_dept" class="form-control select2" style="width: 100%;" onchange="tes()">
                          <option value=""></option>
                          <?php
                          foreach ($kode_departemen->result() as $row){
                          ?>
                          <option value="<?php echo $row->kode_departemen; ?>"> <?php echo $row->kode_departemen; ?>  |   <?php echo $row->nama_departemen; ?></option>
                          <?php } ?>
                        </select>


                        <select name="tujuan_grup" class="form-control select2" style="width: 100%;" >
                          <option value=""></option>
                          <?php
                          foreach ($kode_grup_pe->result() as $row){
                          ?>
                          <optgroup label="<?php echo $row->kode_departemen; ?>">
                            <?php $grupa = explode(',', $row->grup);
                             foreach ($grupa as $kode_grup) { ?>
                              <option value="<?php echo $kode_grup; ?>"> <?php echo $kode_grup; ?>  |  <?php echo $row->nama_grup; ?></option>
                            <?php } ?>
                          </optgroup>
                          <?php } ?>
                        </select>


                        <select name="tujuan_div" class="form-control select2" style="width: 100%;" >
                          <option value=""></option>
                          <?php
                          foreach ($kode_divisi_pe->result() as $row){
                          ?>
                          <optgroup label="<?php echo $row->kode_departemen; ?>">
                            <?php $grup = explode(',', $row->divisi);
                             foreach ($grup as $kode_grup) { ?>
                              <option value="<?php echo $kode_grup; ?>"> <?php echo $kode_grup; ?>  |  <?php echo $row->nama_divisi; ?></option>
                            <?php } ?>
                          </optgroup>
                          <?php } ?>
                        </select>

                        <input type="hidden" name="ditujukan_kepada" class="form-control" id="exampleInputText1" value="<?php echo $ditujukan_kepada; ?>"/>   
                    </div>
                      <div class="form-group">
                        <label>Sifat Dokumen</label>
                        <select class="form-control" name="rahasia" value="<?php echo $old_rahasia; ?>">
                          <option value="<?php echo $old_rahasia; ?>"<?php echo $old_rahasia; ?>></option>
                          <option value="1">Rahasia</option>
                          <option value="0">Biasa</option>
                        </select>
                      </div>
                    <div class="form-group">
                      <input type="hidden" name="nomor_dokumen" class="form-control" id="nomor_dokumen" value="<?php echo $nomor_dokumen; ?>"/>
                    </div>
                    <div class="form-group">
                      <input type="hidden" name="kode_ddk" class="form-control" value="<?php echo $kode_ddk; ?>"/>
                    </div>

                    <div class="form-group">
                    <p> hasil : </p><div id='nomor_dokumen_availability_result'></div>
                    </div>  
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="button" class="btn btn-primary" id="check_nomor_dokumen_availability" onclick="generate()">Generate</button>
                    <button type="submit"  class="btn btn-primary" onclick="return valid()">Simpan</button>
                </div>
                </form>
              </div><!-- /.box -->


            </div><!-- /.col (left) -->
            <div class="col-md-6">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Informasi</h3>
                </div>
                <div class="box-body">
                  <!-- Date range -->
                  <div class="form-group">
                    <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Jenis Dokumen</th>
                        <th>Nomor Terakhir Dipakai</th>
                      </tr>
                    </thead>
                    <tbody>     
                      <?php
                      foreach ($info_jenis->result() as $info){ ?> 
                      <tr>
                        <td><?php echo $info->kode; ?></td>
                        <td><?php echo $info->count; ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  </div><!-- /.form group -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col (right) -->

            <div class="col-md-6">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Preview Hasil</h3>
                </div>
                <div class="box-body">
                  <!-- Date range -->
                  <div class="form-group">
                    <label for="exampleInputText1">Nomor Dokumen</label>
                    <input type="text" class="form-control" id="showNomor" value="<?php echo $nomor_dokumen; ?>" disabled/>
                    <label for="exampleInputText1">Tujuan</label>
                    <input type="text" class="form-control" id="showTujuan" value="<?php echo $ditujukan_kepada; ?>" disabled/>
                        
                  </div><!-- /.form group -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->

        <script type="text/javascript">
          function tes(){
            //alert("test");
          }
          function valid(){
            if(!form1.nomor_dokumen.value){
              alert("data harus terisi dengan benar");
              return false;
            }
          }
          function generate(){

          var checking_html = '<img src="<?php echo base_url(); ?>assets/dist/img/loading.gif" /> Checking...';
            
            //membuat kode pemrakarsa
            kode_grup = document.getElementsByName('kode_grup')[0].value;
            if(kode_grup!=""){
              kode_grup = "-"+kode_grup;
            }
            kode_divisi = document.getElementsByName('kode_divisi')[0].value;
            if(kode_divisi!=""){
              kode_divisi = "-"+kode_divisi;
            }
            kode_departemen = document.getElementsByName('kode_departemen')[0].value;
            form1.pemrakarsa.value = kode_departemen+kode_grup+kode_divisi;
            

            //membuat kode ditujukan kepada
            t_grup = document.getElementsByName('tujuan_grup')[0].value;
            if(t_grup!=""){
              t_grup = "-"+t_grup;
            }
            t_divisi = document.getElementsByName('tujuan_div')[0].value;
            if(t_divisi!=""){
              t_divisi = "-"+t_divisi;
            }
            t_dept = document.getElementsByName('tujuan_dept')[0].value;
            tujuan = t_dept+t_grup+t_divisi;
            form1.ditujukan_kepada.value = tujuan;
            document.getElementById("showTujuan").value = tujuan;

            kode_tahun = document.getElementsByName('kode_tahun')[0].value;
            no_sesuai_jenis = document.getElementsByName('no_sesuai_jenis')[0].value;
            pemrakarsa = document.getElementsByName('pemrakarsa')[0].value;
            kode_jenisdok = document.getElementsByName('kode_jenisdok')[0].value;
            rahasia = document.getElementsByName('rahasia')[0].value;
            if(rahasia =='0'){
              rahasia = 'B';
            }
            else{
              rahasia = 'Rhs';
            }

            //mencari no urut yang mengambil nilai max dari dokumen.
            xcode = kode_tahun+"/"+no_sesuai_jenis+"/"+pemrakarsa+"/"+kode_jenisdok+"/"+rahasia;    
            form1.nomor_dokumen.value = xcode;
            //dicek dulu hasilnya
            $('#nomor_dokumen_availability_result').html(checking_html);
            check_ava();
            //view result
            document.getElementById("showNomor").value = form1.nomor_dokumen.value;
          }
          function check_ava(){
            var checkedId = document.getElementsByName('nomor_dokumen')[0].value;
            jQuery.ajax({
                              type: "POST",
                              url: "<?php echo base_url(); ?>" + "index.php/ddk/checkNomor",
                              dataType: 'json',
                              data: {id: checkedId},
                              success: function(res) {
                                  if (res.result == 1)
                                  {
                                    jQuery("div#nomor_dokumen_availability_result").html('<span class="is_available">' +checkedId + ' is Available</span>');
                                    
                                  }
                                  else{
                                    form1.nomor_dokumen.value= "";
                                    jQuery("div#nomor_dokumen_availability_result").html('<span class="is_not_available">' +checkedId + ' is not Available</span>');
                                  }
                              }
                          });
        }
          function today(){
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth()+1; //January is 0!
                    var yyyy = today.getFullYear();

                    if(dd<10) {
                        dd='0'+dd
                    } 

                    if(mm<10) {
                        mm='0'+mm
                    } 

                    today = yyyy+'/'+mm+'/'+dd;
                    document.getElementsByName('tanggal_dokumen')[0].value = today;
          }
        </script>

<style type='text/css'>
.is_available{
  color:blue;
}
.is_not_available{
  color:red;
}
</style>