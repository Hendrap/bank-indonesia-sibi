
                <div align="center">
                <form method="post" action="<?php echo base_url(); ?>/index.php/ddk/filter">
                        <i class="fa fa-filter"></i> Show filter :
                        <select name="jenis" >
                          <option value="">All</option>
                          <?php
                          foreach ($byjenisdok->result() as $row){
                          ?>
                          <option value="<?php echo $row->kode_jenisdok; ?>"> <?php echo $row->kode_jenisdok; ?></option>
                          <?php } ?>
                          <option value="<?php echo $old_jenis; ?>" selected><?php echo $old_jenis; ?></option>
                        </select>

                        <select name="tahun">
                          <option value="">All</option>
                          <?php
                          foreach ($bytahun->result() as $row){
                          ?>
                          <option value="<?php echo $row->kode_tahun; ?>"> <?php echo $row->kode_tahun; ?></option>
                          <?php } ?>
                          <option value="<?php echo $old_tahun; ?>" selected><?php echo $old_tahun; ?></option>
                        </select>
                        <button type="submit"  class="btn">Filter</button>
                </form>
              </div>

                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nomor</th>
                        <th>Nomor Dokumen Keluar</th>
                        <th>Tanggal Dokumen</th>
                        <th>Perihal</th>
                        <th>Ditujukan kepada</th>
                        <th>Pemrakarsa</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>     
                      <?php
                      $no=1;
                      foreach ($data->result() as $row){ ?> 
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $row->nomor_dokumen; ?></td>
                        <td><?php echo $row->tanggal_dokumen; ?></td>
                        <td><?php echo $row->perihal; ?></td>
                        <td><?php echo $row->ditujukan_kepada; ?></td>
                        <td><?php echo $row->pemrakarsa; ?></td>
                        <td><?php echo $row->keterangan; ?></td>
                        <?php if($this->session->userdata('jabatan')=='pegawai'){ ?>
                        <td>
                          <a href="<?php echo base_url(); ?>index.php/ddk/view/<?php echo $row->kode_ddk; ?>" class="btn btn-default"><i class="fa fa-folder-open-o"></i></a>
                        </td>
                      <?php }
                      else{ ?>
                        <td>
                          <a href="<?php echo base_url(); ?>index.php/ddk/view/<?php echo $row->kode_ddk; ?>" class="btn btn-default"><i class="fa fa-folder-open-o"></i></a>
                          <a href="<?php echo base_url(); ?>index.php/ddk/edit/<?php echo $row->kode_ddk; ?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                          <a href="<?php echo base_url(); ?>index.php/ddk/delete/<?php echo $row->kode_ddk; ?>" class="btn btn-default"><i class="fa fa-trash"></i></a>
                        </td>
                        <?php } ?>
                      </tr>
                      <?php 
                      $no++;} ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nomor</th>
                        <th>Nomor Dokumen Keluar</th>
                        <th>Tanggal Dokumen</th>
                        <th>Perihal</th>
                        <th>Ditujukan kepada</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
                  <div class="margin">
                    <a href="<?php echo base_url();?>index.php/ddk/cetak?jenis=<?php echo $old_jenis; ?>&tahun=<?php echo $old_tahun; ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                  </div>