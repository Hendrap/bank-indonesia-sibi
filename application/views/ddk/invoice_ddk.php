<section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="row" align="left">
      <div class="margin">
        <img src="<?php echo base_url(); ?>/assets/dist/img/logo-name.png" width="300" />
      </div>
      </div>
    </div>
    <!-- title row -->
    <div class="row" align="center">
      <div class="col-xs-12">
        <h2 class="page-header">DAFTAR DOKUMEN KELUAR</h2>
      </div>
    </div>
      <!-- /.col -->
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        Dicetak oleh
        <address>
          <?php foreach ($pegawai->result() as $row) {
            echo "
              <strong> $row->nama_pegawai </strong><br>
              $row->nip <br>
              $row->kode_divisi <br>
            ";
          } ?>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        Filter
        <address>
          Jenis dokumen : <strong><?php echo $jenis; ?></strong><br>
          Tahun Buku    : <?php echo $tahun; ?>
        </address>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table id="example1" class="table table-striped">
                    <thead>
                      <tr>
                        <th>Nomor</th>
                        <th>Nomor Dokumen</th>
                        <th>Tanggal Dokumen</th>
                        <th>Perihal</th>
                        <th>Ditujukan kepada</th>
                        <th>Pemrakarsa</th>
                        <th>Keterangan</th>
                      </tr>
                    </thead>
                    <tbody>     
                      <?php
                      $no=1;
                      foreach ($data->result() as $row){ ?> 
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $row->nomor_dokumen; ?></td>
                        <td><?php echo $row->tanggal_dokumen; ?></td>
                        <td><?php echo $row->perihal; ?></td>
                        <td><?php echo $row->ditujukan_kepada; ?></td>
                        <td><?php echo $row->pemrakarsa; ?></td>
                        <td><?php echo $row->keterangan; ?></td>
                      </tr>
                      <?php 
                      $no++;} ?>
                    </tbody>
                  </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
      <div class="col-xs-12">
        
        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
          Lembar ini hanya untuk internal Bank Indonesia.
        </p>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>