
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nomor</th>
                        <th>Kode Divisi</th>
                        <th>Nama Divisi</th>
                        <th>Departemen</th>
                        <th>Grup</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>     
                      <?php
                      $no=1;
                      foreach ($data->result() as $row){ ?> 
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $row->kode_divisi; ?></td>
                        <td><?php echo $row->nama_divisi; ?></td>
                        <td><?php echo $row->kode_departemen; ?></td>
                        <td><?php echo $row->kode_grup; ?></td>
                        <td>
                          <?php if( ($row->aktif)=='1'){
                          ?>
                            <a href="<?php echo base_url(); ?>index.php/divisi/state/<?php echo $row->kode_divisi; ?>/0"><span class="label label-success">Aktif</span></a>
                          <?php 
                          }else{
                          ?>
                              <a href="<?php echo base_url(); ?>index.php/divisi/state/<?php echo $row->kode_divisi; ?>/1"><span class="label label-danger">Non Aktif</span></a>
                          <?php } ?>
                        </td>
                        <td>
                          <a href="<?php echo base_url(); ?>index.php/divisi/edit/<?php echo $row->kode_divisi; ?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                          <a href="<?php echo base_url(); ?>index.php/divisi/delete/<?php echo $row->kode_divisi; ?>" class="btn btn-default"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                      <?php 
                      $no++;} ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Nomor</th>
                        <th>Kode Divisi</th>
                        <th>Nama Divisi</th>
                        <th>Departemen</th>
                        <th>Grup</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>