          
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Edit Divisi</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?php echo base_url();?>index.php/divisi/simpan"> 
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputText1">Kode Divisi : <?php echo $kode_divisi; ?></label>
                      <input type="hidden" class="form-control" name="kode_divisi" id="exampleInputText1" placeholder="Enter Kode" value="<?php echo $kode_divisi; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Nama Divisi</label>
                      <input type="text" class="form-control" name="nama_divisi" id="exampleInputText2" placeholder="Enter Nama" value="<?php echo $nama_divisi; ?>">
                    </div>
                  </div><!-- /.box-body -->
                  <div class="form-group">
                        <label>Departemen <small>(kosongkan jika tidak ada perubahan)</small></label>
                        <select name="kode_departemen" class="form-control select2" style="width: 100%;" value="<?php echo $old_kode_departemen; ?>">
                          <option value="<?php echo $old_kode_departemen; ?>"></option>
                          <?php
                          foreach ($kode_departemen->result() as $row){
                          ?>
                          <option value="<?php echo $row->kode_departemen; ?>"> <?php echo $row->nama_departemen; ?></option>
                          <?php } ?>
                        </select>
                    </div>
                  <div class="form-group">
                        <label>Grup <small>(kosongkan jika tidak ada perubahan)</small></label>
                        <select name="kode_grup" class="form-control select2" style="width: 100%;" value="<?php echo $old_kode_grup; ?>">
                          <option value="<?php echo $old_kode_grup; ?>"></option>
                          <?php
                          foreach ($kode_grup->result() as $row){
                          ?>
                          <option value="<?php echo $row->kode_grup; ?>"> <?php echo $row->nama_grup; ?></option>
                          <?php } ?>
                        </select>
                    </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                </form>