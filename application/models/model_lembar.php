<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_lembar extends CI_Model{

        private $nama_tabel = 'lembar_disposisi';
        private $primary    = 'nomor_ldp';
        function __construct()
        {
            parent::__construct();
        }
        
    public function getinsert($ddm,$jenislembar)
    {
        $this->db->insert($this->nama_tabel,array(
                                    'nomor_ldp'=>$this->newID(),
                                    'kode_ddm'=>$ddm,
                                    'jenis_lembar'=>$jenislembar));
        
    }
   
    public function ifexist($key)
    {
        $this->db->where('kode_ddm',$key);
        $hasil = $this->db->get($this->nama_tabel);
        if($hasil->num_rows()>0){
            return 0;
        }else{
            return 1;
        }

    }
    public function getjenislembar($key)
    {
        $hasil = $this->db->query("SELECT * FROM lembar_disposisi WHERE kode_ddm = '$key' LIMIT 1");
        return $hasil->row();
    }
    function jumlah_data()
    {
            return $this->db->count_all($this->nama_tabel);
    }
    function newID()
    {
            if(0 == $this->jumlah_data())
               return '1';

           $this->db->select_max('nomor_ldp');
           $d = $this->db->get('lembar_disposisi')->row();
           $id = $d->nomor_ldp;
           $newid = $id +1; 
           return $newid;
    }
     
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */