<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_Security extends CI_model {

    public function getsecurity()
    {
        $username = $this->session->userdata('username');
        if(empty($username))
        {
            $this->session->sess_destroy();
            redirect('login');
        }
    }
    
    public function getlevel()
    {
        $level = $this->session->userdata('jabatan');
        if($level=='pegawai'){
            redirect('home/Error404');
        }
    }

    public function justAdmin(){
        $level = $this->session->userdata('jabatan');
        if($level!='administrator'){
            redirect('home/Error404');
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */