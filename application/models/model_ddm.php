<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_ddm extends CI_Model{

        private $nama_tabel = 'ddm';
        private $primary    = 'kode_ddm';
        function __construct()
        {
            parent::__construct();
        }
    public function view()
    {
        $this->db->order_by('tanggal_masuk','desc');
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function viewbyfilter($jenis,$tahun,$dept,$grup,$divisi)
    {
        $this->db->order_by('kode_ddm','desc');
        
        if(!empty($divisi))
            $this->db->where('divisi',$divisi);
        if(!empty($grup))
            $this->db->where('grup',$grup);
        if(!empty($dept))
            $this->db->where('departemen',$dept);
        if(!empty($tahun))
            $this->db->where('kode_tahun',$tahun);
        if(!empty($jenis))
        $this->db->where('kode_jenisdok',$jenis);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function getdata($key)
    {
        $this->db->where($this->primary,$key);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    
    function getnomor($nomor){
        $this->db->where('nomor_dokumen',$nomor);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function getupdate($key,$data)
    {
        $nomor_doc = explode('/', $data['nomor_dokumen']);

        $tahun_buku = $nomor_doc[0];
        $no_s_jenis = $nomor_doc[1];
        $pemrakarsa = $nomor_doc[2];
        //njaluk dipecah maneh
        $satker = explode('-', $pemrakarsa);
        if(count($satker)==1){
            $dept = $satker;
            $grup = "";
            $divisi = "";
        }
        else{
            if(count($satker)==2){
                $dept = $satker[0];
                $grup = $satker[1];
            }
            else{
                $dept = $satker[0];
                $grup = $satker[1];
                $divisi = $satker[2];
            }
        }
            
        $jenis_dokumen = $nomor_doc[3];
        $rahasia = $nomor_doc[4];
        if($rahasia=='B'){
            $rahasia=0;
        }
        else{
            $rahasia=1;
        }
        
        $this->db->where($this->primary,$key);
        $this->db->update($this->nama_tabel,array(
                            'nomor_dokumen'=>$data['nomor_dokumen'],
                            'no_sesuai_jenis'=>$no_s_jenis,
                            'tanggal_dokumen'=>$data['tanggal_dokumen'],
                            'tanggal_masuk'=>$data['tanggal_masuk'],
                            'perihal'=>$data['perihal'],
                            'kode_tahun'=>$tahun_buku,
                            'pemrakarsa'=>$pemrakarsa,
                            'departemen'=>$dept,
                            'grup'=>$grup,
                            'divisi'=>$divisi,
                            'rahasia'=>$rahasia,
                            'kode_jenisdok'=>$jenis_dokumen,
                            'ditujukan_kepada'=>$data['ditujukan_kepada'],
                            'keterangan'=>$data['keterangan']));
    }
    
    public function getinsert($data)
    {
        $nomor_doc = explode('/', $data['nomor_dokumen']);

        $tahun_buku = $nomor_doc[0];
        $no_s_jenis = $nomor_doc[1];
        $pemrakarsa = $nomor_doc[2];
        $satker = explode('-', $pemrakarsa);
        if(count($satker)==1){
            $dept = $satker[0];
            $grup = "";
            $divisi = "";
        }
        else{
            if(count($satker)==2){
                $dept = $satker[0];
                $grup = $satker[1];
                $divisi = "";
            }
            else{
                $dept = $satker[0];
                $grup = $satker[1];
                $divisi = $satker[2];
            }
        }
        $jenis_dokumen = $nomor_doc[3];
        $rahasia = $nomor_doc[4];
        if($rahasia=='B'){
            $rahasia=0;
        }
        else{
            $rahasia=1;
        }
        $this->db->insert($this->nama_tabel,array(
                            'kode_ddm'=>$data['kode_ddm'],
                            'nomor_dokumen'=>$data['nomor_dokumen'],
                            'no_sesuai_jenis'=>$no_s_jenis,
                            'tanggal_dokumen'=>$data['tanggal_dokumen'],
                            'tanggal_masuk'=>$data['tanggal_masuk'],
                            'perihal'=>$data['perihal'],
                            'kode_tahun'=>$tahun_buku,
                            'pemrakarsa'=>$pemrakarsa,
                            'departemen'=>$dept,
                            'grup'=>$grup,
                            'divisi'=>$divisi,
                            'rahasia'=>$rahasia,
                            'kode_jenisdok'=>$jenis_dokumen,
                            'ditujukan_kepada'=>$data['ditujukan_kepada'],
                            'keterangan'=>$data['keterangan']));
    }
    
    public function getdelete($key)
    { 
        $this->session->set_flashdata('info','data telah terhapus dari daftar');
        $this->db->where($this->primary, $key);
        return $this->db->delete($this->nama_tabel);
    }
    function jumlah_data()
    {
            return $this->db->count_all($this->nama_tabel);
    }
    function newID()
    {
            if(0 == $this->jumlah_data())
               return '1';

           $this->db->select_max('kode_ddm');
           $d = $this->db->get('ddm')->row();
           $id = $d->kode_ddm;
           $newid = $id +1; 
           return $newid;
    }
    
    function getNoDokumen($key){
        $this->db->select('nomor_dokumen');
        $this->db->where($this->primary,$key);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */