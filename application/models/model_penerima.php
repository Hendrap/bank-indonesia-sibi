<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_penerima extends CI_Model{

        private $nama_tabel = 'penerima_disposisi';
        private $primary    = 'kode_penerima';
        function __construct()
        {
            parent::__construct();
        }
    public function view($key)
    {
        $this->db->order_by($this->primary,'desc');
        $this->db->where('kode_disposisi',$key);
        $this->db->join('pegawai', 'pegawai.nip = penerima_disposisi.nip');
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    
    public function getinsert($disposisi,$penerima)
    {
        $i = 0;
        while($i < count($penerima)){
            $this->db->insert($this->nama_tabel,array(
                            'kode_penerima'=>$this->newID(),
                            'kode_disposisi'=>$disposisi,
                            'nip'=>$penerima[$i],
                            'cek'=>0));
            $i++;
        }
    }
    
    public function getdelete($key)
    { 
        $this->session->set_flashdata('info','data telah terhapus dari daftar');
        $this->db->where($this->primary, $key);
        return $this->db->delete($this->nama_tabel);
    }
    
    function jumlah_data()
    {
            return $this->db->count_all($this->nama_tabel);
    }
    function newID()
    {
            if(0 == $this->jumlah_data())
               return '1';

           $this->db->select_max($this->primary);
           $d = $this->db->get($this->nama_tabel)->row();
           $id = $d->kode_penerima;
           $newid = $id +1; 
           return $newid;
    }

    public function changeStatus($key)
    { 
        $this->session->set_flashdata('info','disposisi telah diterima');
        $this->db->where($this->primary, $key);
        $this->db->update($this->nama_tabel, array('cek'=>1));

    }    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */