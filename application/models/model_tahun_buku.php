<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_tahun_buku extends CI_Model{

        private $nama_tabel = 'tahun_buku';
        private $primary    = 'kode_tahun';
        function __construct()
        {
            parent::__construct();
        }
    public function view()
    {
        $this->db->order_by('tahun_buku','asc');
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function viewAktif()
    {
        $this->db->where('aktif',1);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function getdata($key)
    {
        $this->db->where($this->primary,$key);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    
    public function getupdate($key,$data)
    {
        $this->db->where($this->primary,$key);
        $this->db->update($this->nama_tabel,$data);
    }
    
    public function getinsert($data)
    {
        $this->db->insert($this->nama_tabel,$data);
    }
    
    public function getdelete($key)
    { 
        $this->session->set_flashdata('info','data telah terhapus dari daftar');
        $this->db->where($this->primary, $key);
        return $this->db->delete($this->nama_tabel);
    }
    function jumlah_data()
    {
            return $this->db->count_all($this->nama_tabel);
    }
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */