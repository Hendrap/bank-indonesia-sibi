<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_pegawai extends CI_Model{

        private $nama_tabel = 'pegawai';
        private $primary    = 'nip';
        function __construct()
        {
            parent::__construct();
        }
    public function view()
    {
        $this->db->order_by('nama_pegawai','asc');
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function viewAktif()
    {
        $this->db->order_by('nama_pegawai','asc');
        $this->db->where_not_in('jabatan','administrator');
        $this->db->where('aktif','1');
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function viewMy($nip)
    {
        $this->db->where($this->primary,$nip);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }

    public function viewFriend($divisi,$nip)
    {
        if($divisi){
            $this->db->where('kode_divisi',$divisi);
        }
        $this->db->where_not_in($this->primary,$nip);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function getdata($key)
    {
        $this->db->where($this->primary,$key);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function getnama($key)
    {
        $this->db->where($this->primary,$key);
        $this->db->select('nama_pegawai');
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    
    public function getupdate($key,$data)
    {
        if(!$data['password']){
            $this->db->where($this->primary,$key);
            $this->db->update($this->nama_tabel,array(
                            'nip'=>$data['nip'],
                            'nama_pegawai'=>$data['nama_pegawai'],
                            'username'=>$data['username'],
                            'jabatan'=>$data['jabatan'],
                            'kode_divisi'=>$data['kode_divisi'],
                            'foto'=>$data['foto'],
                            'thumb'=>$data['thumb'],
                            'aktif'=>$data['aktif']));
        }
        else{
            $this->db->where($this->primary,$key);
            $this->db->update($this->nama_tabel,array(
                            'nip'=>$data['nip'],
                            'nama_pegawai'=>$data['nama_pegawai'],
                            'username'=>$data['username'],
                            'password'=>md5($data['password']),
                            'jabatan'=>$data['jabatan'],
                            'kode_divisi'=>$data['kode_divisi'],
                            'foto'=>$data['foto'],
                            'thumb'=>$data['thumb'],
                            'aktif'=>$data['aktif']));
        }
        
    }

    public function setthumbname($name){
            $id = explode('.', $name);
           $id[0] = $id[0]."_thumb" ;
           $id = implode('.', $id);
           return $id;
    }
    
    public function getinsert($data)
    {
        $this->db->insert($this->nama_tabel,array(
                            'nip'=>$data['nip'],
                            'nama_pegawai'=>$data['nama_pegawai'],
                            'username'=>$data['username'],
                            'password'=>md5($data['password']),
                            'jabatan'=>$data['jabatan'],
                            'kode_divisi'=>$data['kode_divisi'],
                            'foto'=>$data['foto'],
                            'thumb'=>$data['thumb'],
                            'aktif'=>$data['aktif']));
    }
    
    public function getdelete($key)
    { 
        $this->session->set_flashdata('info','data telah terhapus dari daftar');
        $this->db->where($this->primary, $key);
        return $this->db->delete($this->nama_tabel);
    }
    public function changeStatus($key,$stat)
    { 
        $this->session->set_flashdata('info','status data telah diubah');
        $this->db->where($this->primary, $key);
        return $this->db->update($this->nama_tabel, array('aktif'=>$stat));
    }
    function jumlah_data()
    {
            return $this->db->count_all($this->nama_tabel);
    }
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */