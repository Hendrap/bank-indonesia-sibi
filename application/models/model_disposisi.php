<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_disposisi extends CI_Model{

        private $nama_tabel = 'disposisi';
        private $primary    = 'kode_disposisi';
        function __construct()
        {
            parent::__construct();
        }
    public function view($key)
    {
        $this->db->select('*');
        $this->db->from('disposisi');
        $this->db->join('pegawai', 'pegawai.nip = disposisi.dari');
        $this->db->where('disposisi.kode_ddm',$key);
        $this->db->order_by('tanggal','DESC');
        $hasil = $this->db->get();
        return $hasil;
    }
    public function getmydisposisi()
    {
        $me = $this->session->userdata('nip');
        $query = "SELECT 
                    pegawai.foto as foto, pegawai.nama_pegawai as nama_pegawai,
                    ddm.nomor_dokumen as nomor_dokumen, ddm.perihal as perihal,
                    disposisi.kode_disposisi, disposisi.disposisi, disposisi.pesan_singkat, disposisi.tanggal, disposisi.kode_ddm,
                    penerima_disposisi.cek as cek, penerima_disposisi.kode_penerima

                    from disposisi
                    left join pegawai
                    on disposisi.dari = pegawai.nip
                    left join ddm
                    on disposisi.kode_ddm = ddm.kode_ddm
                    left join penerima_disposisi
                    on disposisi.kode_disposisi = penerima_disposisi.kode_disposisi
                    where penerima_disposisi.nip= '$me'
                    order by disposisi.tanggal desc
                    LIMIT 10";
        $hasil = $this->db->query($query);
        return $hasil;
    }

    public function getmyact()
    {
        //menampilkan kepada siapa siapa saja saya memberi disposisi
        $me = $this->session->userdata('nip');
        $query = "SELECT 
                    pegawai.nama_pegawai as nama_pegawai,
                    ddm.nomor_dokumen as nomor_dokumen, ddm.perihal as perihal,
                    disposisi.kode_disposisi, disposisi.tanggal, penerima_disposisi.cek, disposisi.kode_ddm

                    from disposisi
                    left join penerima_disposisi
                    on penerima_disposisi.kode_disposisi = disposisi.kode_disposisi
                    left join pegawai
                    on penerima_disposisi.nip = pegawai.nip
                    left join ddm
                    on disposisi.kode_ddm = ddm.kode_ddm
                    where disposisi.dari= '$me'
                    order by disposisi.tanggal desc
                    LIMIT 20";
        $hasil = $this->db->query($query);
        return $hasil;
    }

    public function hakforAdd($kode_ddm){
        $me =$this->session->userdata('nip');

        $query = "SELECT distinct(nip) from penerima_disposisi where kode_disposisi IN (select kode_disposisi from disposisi where kode_ddm = '$kode_ddm') and nip = '$me' ";
        return $this->db->query($query);
        
    }
    public function getdata($key)
    {
        $this->db->where($this->primary,$key);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function getMyTask(){
        $mynip = $this->session->userdata('nip');
        $this->db->where('penerima',$mynip);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;   
    }
    public function getupdate($key,$data)
    {
        $this->db->where($this->primary,$key);
        $this->db->update($this->nama_tabel,$data);
    }
    
    public function getinsert($data)
    {
        $this->db->insert($this->nama_tabel,$data);
    }
    
    public function getdelete($key)
    { 
        $this->session->set_flashdata('info','data telah terhapus dari daftar');
        $this->db->where($this->primary, $key);
        return $this->db->delete($this->nama_tabel);
    }
    function jumlah_data_byddm($kode_ddm)
    {
        $this->db->where('kode_ddm',$kode_ddm);
        return $this->db->count_all($this->nama_tabel);
    }
    function jumlah_data()
    {
            return $this->db->count_all($this->nama_tabel);
    }
    function newID($kode_ddm)
        {
            if(0 == $this->jumlah_data_byddm($kode_ddm)){
                $id[0] = 'D';
                $id[1] = '1';
                $id[2] = $kode_ddm;
                $id = implode('_', $id);
            }
            else{//mengambil kode disposisi maksimal berdasarkan kode_ddm nya
               $this->db->where('kode_ddm',$kode_ddm);
               $this->db->select_max($this->primary);
               $d = $this->db->get($this->nama_tabel)->row();

               // dipecah
               $id = explode('_', "$d->kode_disposisi");
               //error handling
               if(!isset($id[1])){
                $id[1]=null;
               }
               //end error handling
               $id[0] = 'D';
               $id[1] = $id[1] + 1;
               $id[2] = $kode_ddm;
               $id = implode('_', $id);
            }
            return $id;
    }
    
    function getKodeDdm($kode){
        $id = explode('_', $kode);
        return $id[2];
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */