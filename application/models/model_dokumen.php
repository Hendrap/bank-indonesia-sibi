<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_dokumen extends CI_Model{

        private $nama_tabel = 'ddk';
        private $primary    = 'kode_ddk';
        function __construct()
        {
            parent::__construct();
        }
    public function view()
    {
        $this->db->order_by('kode_ddk','asc');
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function getdata($key)
    {
        $this->db->where($this->primary,$key);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    
    public function getupdate($key,$data)
    {
        $this->db->where($this->primary,$key);
        $this->db->update($this->nama_tabel,$data);
    }
    
    public function getinsert($data)
    {
        $this->db->insert($this->nama_tabel,$data);
    }
    
    public function getdelete($key)
    { 
        $this->session->set_flashdata('info','data telah terhapus dari daftar');
        $this->db->where($this->primary, $key);
        return $this->db->delete($this->nama_tabel);
    }
    function jumlah_data()
    {
            return $this->db->count_all($this->nama_tabel);
    }
    function newID()
        {
            if(0 == $this->jumlah_data())
               return '1';

           $this->db->select_max('kode_ddk');
           $d = $this->db->get('ddk')->row();
           $id = $d->kode_ddk;
           $newid = $id +1; 
           return $newid;
       }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */