<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_login extends CI_Model {
    public function getlogin($u,$p)
    {
        $pwd = md5($p);
        $this->db->where('username',$u);
        $this->db->where('password',$pwd);
        $this->db->where('aktif','1');
        $query = $this->db->get('pegawai');
        if($query->num_rows()>0)
        {
            foreach ($query->result() as $row)
            {
                $sess = array('nip'        => $row->nip,
                              'username'        => $row->username,
                              'kode_divisi'    => $row->kode_divisi,
                              'jabatan'    => $row->jabatan,
                              'nama_pegawai'    => $row->nama_pegawai,
                              'foto' => $row->foto,
                              'thumb'    => $row->thumb); 
                $this->session->set_userdata($sess);
                redirect('home');
            }
        }
        else
        {
            $this->session->set_flashdata('info','maaf username atau password salah');
            redirect('login');
        }
    } 

    public function apilogin($u,$p)
    {
        $pwd = md5($p);
        $this->db->where('username',$u);
        $this->db->where('password',$pwd);
        $this->db->where('aktif','1');
        $query = $this->db->get('pegawai');
        if($query->num_rows()>0)
        {
            foreach ($query->result() as $row)
            {
                $sess = array('nip'        => $row->nip,
                              'username'        => $row->username,
                              'kode_divisi'    => $row->kode_divisi,
                              'jabatan'    => $row->jabatan,
                              'nama_pegawai'    => $row->nama_pegawai,
                              'foto' => $row->foto,
                              'thumb'    => $row->thumb); 
            }
            $token = md5(rand(1,999));
            $content['id'] = "200";
            $content['msg'] = "success";
            $content['token'] = $token;
            $content['data'] = $query->result();

            header('Content-Type: application/json');
            echo json_encode($content);

        }
        else
        {
            $content['id'] = "200";
            $content['msg'] = "success";
            $content['data'] = "";
            header('Content-Type: application/json');
            echo json_encode($content);
        }
    }  
    
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */