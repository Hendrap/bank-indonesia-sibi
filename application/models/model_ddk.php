<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_ddk extends CI_Model{

        private $nama_tabel = 'ddk';
        private $primary    = 'kode_ddk';
        function __construct()
        {
            parent::__construct();
        }
    public function view()
    {
        $this->db->order_by('kode_ddk','desc');
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function viewbyfilter($jenis,$tahun)
    {
        $this->db->order_by('kode_ddk','desc');
        
        if(!empty($tahun))
            $this->db->where('kode_tahun',$tahun);
        if(!empty($jenis))
        $this->db->where('kode_jenisdok',$jenis);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function getdata($key)
    {
        $this->db->where($this->primary,$key);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    
    public function getupdate($key,$data)
    {
        $this->db->where($this->primary,$key);
        $this->db->update($this->nama_tabel,$data);
    }
    
    public function getinsert($data)
    {
        $this->db->insert($this->nama_tabel,$data);
    }
    
    public function getdelete($key)
    { 
        $this->session->set_flashdata('info','data telah terhapus dari daftar');
        $this->db->where($this->primary, $key);
        return $this->db->delete($this->nama_tabel);
    }
    function jumlah_data()
    {
            return $this->db->count_all($this->nama_tabel);
    }
    function newID()
    {
            if(0 == $this->jumlah_data())
               return '1';

           $this->db->select_max($this->primary);
           $d = $this->db->get($this->nama_tabel)->row();
           $id = $d->kode_ddk;
           $newid = $id +1; 
           return $newid;
    }
    function getInfoJenis(){

        $query = "SELECT j.kode_jenisdok as kode, max(d.no_sesuai_jenis) as count from ddk d, jenis_dokumen j where j.kode_jenisdok=d.kode_jenisdok group by j.kode_jenisdok";
        $hasil = $this->db->query($query);
        return $hasil;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */