<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_grup extends CI_Model{

        private $nama_tabel = 'grup';
        private $primary    = 'kode_grup';
        function __construct()
        {
            parent::__construct();
        }
    public function view()
    {
        $this->db->order_by('nama_grup','asc');
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function viewAktif()
    {
        $this->db->where('aktif',1);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function getdata($key)
    {
        $this->db->where($this->primary,$key);
        $this->db->where('aktif',1);
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    
    public function getOrder()
    {
        $this->db->select('kode_departemen,GROUP_CONCAT(kode_grup) AS grup, nama_grup');
        $this->db->where('aktif',1);
        $this->db->group_by('kode_departemen');
        $hasil = $this->db->get($this->nama_tabel);
        return $hasil;
    }
    public function getupdate($key,$data)
    {
        $this->db->where($this->primary,$key);
        $this->db->update($this->nama_tabel,$data);
    }
    
    public function getinsert($data)
    {
        $this->db->insert($this->nama_tabel,$data);
    }
    
    public function getdelete($key)
    { 
        $this->session->set_flashdata('info','data telah terhapus dari daftar');
        $this->db->where($this->primary, $key);
        return $this->db->delete($this->nama_tabel);
    }
    public function changeStatus($key,$stat)
    { 
        $this->session->set_flashdata('info','status data telah diubah');
        $this->db->where($this->primary, $key);
        return $this->db->update($this->nama_tabel, array('aktif'=>$stat));
    }
    function jumlah_data()
    {
            return $this->db->count_all($this->nama_tabel);
    }
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */