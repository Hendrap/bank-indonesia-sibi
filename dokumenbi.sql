-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2016 at 02:10 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dokumenbi`
--

-- --------------------------------------------------------

--
-- Table structure for table `ddk`
--

CREATE TABLE IF NOT EXISTS `ddk` (
  `kode_ddk` varchar(8) NOT NULL,
  `nomor_dokumen` varchar(50) NOT NULL,
  `no_sesuai_jenis` varchar(4) NOT NULL,
  `tanggal_dokumen` date NOT NULL,
  `perihal` text NOT NULL,
  `kode_tahun` int(2) NOT NULL,
  `pemrakarsa` varchar(20) NOT NULL,
  `rahasia` int(1) NOT NULL,
  `kode_jenisdok` varchar(10) NOT NULL,
  `ditujukan_kepada` varchar(20) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`kode_ddk`),
  UNIQUE KEY `nomor_dokumen` (`nomor_dokumen`),
  KEY `kode_tahun` (`kode_tahun`),
  KEY `kode_divisi` (`pemrakarsa`,`rahasia`,`kode_jenisdok`),
  KEY `kode_tahun_2` (`kode_tahun`),
  KEY `kode_jenisdok` (`kode_jenisdok`),
  KEY `kode_sifatdok` (`rahasia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ddk`
--

INSERT INTO `ddk` (`kode_ddk`, `nomor_dokumen`, `no_sesuai_jenis`, `tanggal_dokumen`, `perihal`, `kode_tahun`, `pemrakarsa`, `rahasia`, `kode_jenisdok`, `ditujukan_kepada`, `keterangan`) VALUES
('1', '18/1/DPSI/M.01/Rhs', '1', '2016-02-13', 'Rapat mendadak', 18, 'DPSI', 1, 'M.01', '', ''),
('2', '18/1/DPSI/BA/Rhs', '1', '2016-02-19', 'adasd', 18, 'DPSI', 0, 'BA', 'BINS-GrA-MPSI', 'adsada');

-- --------------------------------------------------------

--
-- Table structure for table `ddm`
--

CREATE TABLE IF NOT EXISTS `ddm` (
  `kode_ddm` varchar(8) NOT NULL,
  `nomor_dokumen` varchar(50) NOT NULL,
  `no_sesuai_jenis` varchar(4) NOT NULL,
  `tanggal_dokumen` date NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `perihal` text NOT NULL,
  `kode_tahun` int(2) NOT NULL,
  `pemrakarsa` varchar(40) NOT NULL,
  `departemen` varchar(15) NOT NULL,
  `grup` varchar(15) NOT NULL,
  `divisi` varchar(15) NOT NULL,
  `rahasia` int(1) NOT NULL,
  `kode_jenisdok` varchar(10) NOT NULL,
  `ditujukan_kepada` text NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`kode_ddm`),
  UNIQUE KEY `nomor_dokumen` (`nomor_dokumen`),
  KEY `kode_tahun` (`kode_tahun`),
  KEY `kode_divisi` (`pemrakarsa`,`rahasia`,`kode_jenisdok`),
  KEY `kode_tahun_2` (`kode_tahun`),
  KEY `kode_jenisdok` (`kode_jenisdok`),
  KEY `kode_sifatdok` (`rahasia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ddm`
--

INSERT INTO `ddm` (`kode_ddm`, `nomor_dokumen`, `no_sesuai_jenis`, `tanggal_dokumen`, `tanggal_masuk`, `perihal`, `kode_tahun`, `pemrakarsa`, `departemen`, `grup`, `divisi`, `rahasia`, `kode_jenisdok`, `ditujukan_kepada`, `keterangan`) VALUES
('3', '18/6/DPSI-GKSI-SKTI/M.02/B', '6', '2016-02-17', '2016-02-17', 'asdad', 18, 'DPSI-GKSI-SKTI', 'DPSI', 'GKSI', 'SKTI', 0, 'M.02', 'asda', 'asd'),
('4', '18/4/DPSI-GKSI-SKTI/M.02/B', '4', '2016-02-18', '2016-02-18', 'tidak diketahui', 18, 'DPSI-GKSI-SKTI', 'DPSI', 'GKSI', 'SKTI', 0, 'M.02', '', ''),
('5', '18/3/DPSI-GKSI-MPSI/M.02/B', '3', '2016-02-18', '2016-02-18', 'kakakaak', 18, 'DPSI-GKSI-MPSI', 'DPSI', 'GKSI', 'MPSI', 0, 'M.02', 'akakak', ''),
('6', '18/127/DMST/M.01/B', '127', '2016-02-18', '2016-02-18', '2', 18, 'DMST', 'DMST', '', '', 0, 'M.01', '2', '2'),
('7', '18/123/DMST-SDM/M.01/B', '123', '2016-02-18', '2016-02-18', 'uup', 18, 'DMST-SDM', 'DMST', 'SDM', '', 0, 'M.01', 'oipop', 'ip'),
('8', '18/2/DPSI-GKSI/M.02/B', '2', '2016-02-18', '2016-02-18', 'coba1', 18, 'DPSI-GKSI', 'DPSI', 'GKSI', '', 0, 'M.02', 'akak', 'sdad'),
('9', '18/127/DMST-GKSI/M.01/B', '127', '2016-02-18', '2016-02-18', 'kkoo', 18, 'DMST-GKSI', 'DMST', 'GKSI', '', 0, 'M.01', 'koko', 'koko');

-- --------------------------------------------------------

--
-- Table structure for table `departemen`
--

CREATE TABLE IF NOT EXISTS `departemen` (
  `kode_departemen` varchar(6) NOT NULL,
  `nama_departemen` text NOT NULL,
  `aktif` int(1) NOT NULL,
  PRIMARY KEY (`kode_departemen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departemen`
--

INSERT INTO `departemen` (`kode_departemen`, `nama_departemen`, `aktif`) VALUES
('BINS', 'INSTITUT BANK INDONESIA / BI INSTITUTE', 1),
('DAI', 'DEPARTEMEN AUDIT INTERN', 1),
('DHk', 'DEPARTEMEN HUKUM', 1),
('DInt', 'DEPARTEMEN INTERNASIONAL', 1),
('DKEM', 'DEPARTEMEN KEBIJAKAN EKONOMI DAN MONETER', 1),
('DKI', 'DEPARTEMEN KEUANGAN INTERN', 1),
('DKMP', 'DEPARTEMEN KEBIJAKAN MAKROPRUDENSIAL', 1),
('Dkom', 'DEPARTEMEN KOMUNIKASI', 1),
('DKSP', 'DEPARTEMEN KEBIJAKAN DAN PENGAWASAN SISTEM PEMBAYARAN', 1),
('DLP', 'DEPARTEMEN LOGISTIK DAN PENGAMANAN', 1),
('DMR', 'DEPARTEMEN MANAJEMEN RESIKO', 1),
('DMST', 'DEPARTEMEN MANAJEMEN STRATEGIS DAN TATA KELOLA', 1),
('DPA', 'DEPARTEMEN PENGOLAHAN ASET', 1),
('DPD', 'DEPARTEMEN PENGOLAHAN DEVISA', 1),
('DPKL', 'DEPARTEMEN PENGOLAHAN DAN KEPATUHAN LAPORAN', 1),
('DPM', 'DEPARTEMEN PENGOLAHAN MONETER', 1),
('DPSI', 'DEPARTEMEN PENGOLAHAN SISTEM INFORMASI', 1),
('DPSP', 'DEPARTEMEN PENYELENGGARAAN SISTEM PEMBAYARAN', 1),
('DPTP', 'DEPARTEMEN PENGOLAHAN PINJAMAN DAN TRANSAKSI PEMER', 1),
('DPU', 'DEPARTEMEN PENGOLAHAN UANG', 1),
('DPUM', 'DEPARTEMEN PENGEMBANGAN UMKM', 1),
('DR1', 'DEPARTEMEN REGIONAL 1', 1),
('DR2', 'DEPARTEMEN REGIONAL II', 1),
('DR3', 'DEPARTEMEN REGIONAL III', 1),
('DR4', 'DEPARTEMEN REGIONAL IV', 1),
('DSDM', 'DEPARTEMEN SUMBER DAYA MANUSIA', 1),
('DSSK', 'DEPARTEMEN SURVEILLANCE SISTEM KEUANGAN', 1),
('DSta', 'DEPARTEMEN STATISIK', 1),
('PPTBI', 'PUSAT PROGRAM TRANFORMASI BANK INDONESIA', 1),
('PRES', 'PUSAT RISET DAN EDUKASI BANK SENTRAL', 1);

-- --------------------------------------------------------

--
-- Table structure for table `disposisi`
--

CREATE TABLE IF NOT EXISTS `disposisi` (
  `kode_disposisi` varchar(10) NOT NULL,
  `kode_ddm` varchar(8) NOT NULL,
  `dari` varchar(11) NOT NULL,
  `penerima` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `disposisi` text NOT NULL,
  `pesan_singkat` text NOT NULL,
  PRIMARY KEY (`kode_disposisi`),
  KEY `kode_ddk` (`kode_ddm`),
  KEY `nip` (`penerima`),
  KEY `dari` (`dari`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disposisi`
--

INSERT INTO `disposisi` (`kode_disposisi`, `kode_ddm`, `dari`, `penerima`, `tanggal`, `disposisi`, `pesan_singkat`) VALUES
('D_1_3', '3', '12567', 'BAHARI PRIYATNA', '2016-02-17', 'Untuk diselenggarakan sesuai ketentuan yang berlaku', ''),
('D_1_4', '4', '1311414', 'TJATUR FADJAR M.', '2016-02-18', '', ''),
('D_1_9', '9', '1311414', 'TJATUR FADJAR M.', '2016-02-18', 'Untuk diteliti dan pendapat', ''),
('D_2_3', '3', '11711', 'CANDRA TRI SURGO', '2016-02-17', 'Untuk diteliti dan pendapat', ''),
('D_2_4', '4', '12567', 'BAHARI PRIYATNA', '2016-02-18', 'Untuk diteliti dan pendapat', ''),
('D_2_9', '9', '12567', 'CANDRA TRI SURGO', '2016-02-18', 'Untuk diteliti dan pendapat', '');

-- --------------------------------------------------------

--
-- Table structure for table `divisi`
--

CREATE TABLE IF NOT EXISTS `divisi` (
  `kode_divisi` varchar(15) NOT NULL,
  `nama_divisi` text NOT NULL,
  `kode_departemen` varchar(6) NOT NULL DEFAULT 'DPSI',
  `kode_grup` varchar(6) NOT NULL,
  `aktif` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`kode_divisi`),
  KEY `kode_dept` (`kode_departemen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `divisi`
--

INSERT INTO `divisi` (`kode_divisi`, `nama_divisi`, `kode_departemen`, `kode_grup`, `aktif`) VALUES
('MPSI', 'Manajemen Program,  Risiko SI, dan Manajemen Inter', 'DPSI', 'GKSI', 1),
('PASI', 'Pengelolaan Aset dan Layanan Sistem Informasi', 'DPSI', 'GPLSI', 1),
('PKSI', 'Pengawasan Kualitas Sistem Informasi', 'DPSI', 'GKSI', 1),
('POSI', 'Pengelolaan Operasional Sistem Informasi', 'DPSI', 'GPLSI', 1),
('PPA-LS', 'Pengembangan dan Pemeliharaan Aplikasi Bidang Lint', 'DPSI', 'GPSI', 1),
('PPA-MI', 'Pengembangan dan Pemeliharaan Aplikasi Bidang Mana', 'DPSI', 'GPSI', 1),
('PPA-MO', 'Pengembangan dan Pemeliharaan Aplikasi Bidang Mone', 'DPSI', 'GPSI', 1),
('PPA-PB & SSK', 'Pengembangan dan Pemeliharaan Aplikasi Bidang Perb', 'DPSI', 'GPSI', 1),
('PPA-SP', 'Pengembangan dan Pemeliharaan Aplikasi Bidang Sist', 'DPSI', 'GPSI', 1),
('PSSI', 'Pengelolaan Sarana Sistem Informasi', 'DPSI', 'GPLSI', 1),
('SKMI', 'Strategi dan Kebijakan Manajemen Informasi', 'DPSI', 'GPPSI', 1),
('SKTI', 'Strategi Tranformasi dan Kebijakan Sistem Informas', 'DPSI', 'GKSI', 1);

-- --------------------------------------------------------

--
-- Table structure for table `grup`
--

CREATE TABLE IF NOT EXISTS `grup` (
  `kode_grup` varchar(6) NOT NULL,
  `nama_grup` text NOT NULL,
  `kode_departemen` varchar(8) NOT NULL,
  `aktif` int(1) NOT NULL,
  PRIMARY KEY (`kode_grup`),
  KEY `kode_departemen` (`kode_departemen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grup`
--

INSERT INTO `grup` (`kode_grup`, `nama_grup`, `kode_departemen`, `aktif`) VALUES
('GADv', 'Grup Analisis dan Pengolahan Devisa Eksternal', 'DPD', 1),
('GAE', 'Grup Asesmen Ekonomi', 'DKEM', 1),
('GAKP', 'Grup Asesmen dan Rekomendasi Kebijakan Makroprudensial', 'DKMP', 1),
('GAPM', 'Grup Optimilasi Aset dan Pengolahan Museum', 'DPA', 1),
('GEK', 'Grup Edukasi Kebanksentralan', 'PRES', 1),
('GKHI', 'Grup Kebijakan dan Hubungan Internasional', 'DInt', 1),
('GKM', 'Grup Kebijakan Moneter', 'DKEM', 1),
('GKOS', 'Grup Kebijakan Organisasi dan SDM', 'DSDM', 1),
('GKPU', 'Grup Kebijakan Pengolahan Uang', 'DPU', 1),
('GKSI', 'Grup Strategi dan Kebijakan Sistem Informasi', 'DPSI', 1),
('GKSP', 'Grup Kebijakan dan Perizinan Sistem Pembayaran', 'DKSP', 1),
('GMDv', 'Grup Manajemen Devisa', 'DPD', 1),
('GMS', 'Grup Manajemen Strategis', 'DMST', 1),
('GNP', 'Grup Neraca Pembayaran dan Pengembangan Statistik', 'DSta', 1),
('GOpM', 'Grup Operasi Moneter', 'DPM', 1),
('GP2A', 'Grup Perencanaan dan Penyelesaian Aset', 'DPA', 1),
('GP2L', 'Grup Pengolahan dan Pengawasan Laporan 2', 'DPKL', 1),
('GP2S', 'Grup Pengembangan dan Pemeliharaan Sistem Pembayaran BI', 'DPSP', 1),
('GP3K', 'Grup Pengaturan, Perencanaan dan Pelaporan Keuangan', 'DKI', 1),
('GP3M', 'Grup Pengembangan dan Pengaturan Pengolahan Moneter', 'DPM', 1),
('GPam', 'Grup Pengamanan dan Arsip', 'DLP', 1),
('GPH', 'Grup Penasehat Hukum', 'DHk', 1),
('GPHL', 'Grup Penasehat Hukum, Peradilan dan legislasi', 'DHk', 1),
('GPK', 'Grup Perencanaan Komunikasi', 'Dkom', 1),
('GPL', 'Grup Pelaksanaan Logistik', 'DLP', 1),
('GPLSI', 'Grup Pengelolaan Operasional, Layanan, Sarana dan Aset SI', 'DPSI', 1),
('GPOM', 'Grup Pendukung Operasi Moneter', 'DPM', 1),
('GPR', 'Grup Pengolahan Relasi', 'Dkom', 1),
('GPrL', 'Grup Perencanaan Logistik', 'DLP', 1),
('GPSI', 'Grup Pengembangan dan Pemeliharaan Sistem Informasi', 'DPSI', 1),
('GPSP', 'Grup Pengawasan Sistem Pembayaran dan Pedagang Valuta Asing', 'DKSP', 1),
('GPTP', 'Grup Pengolahan Transaksi Pemerintah dan BI', 'DPTP', 1),
('GrA', 'Grup Audit', 'DAI', 1),
('GRE', 'Grup Riset Ekonomi', 'DKEM', 1),
('GRK', 'Grup Riset Kebanksentralan', 'PRES', 1),
('GRMP', 'Grup Riset Pengaturan Makroprudensial', 'DKMP', 1),
('GSD', 'Grup Statistik Domestik', 'DSta', 1),
('GSDG', 'Grup Sekretariat Dewan Gubernur', 'DMST', 1),
('GSI', 'Grup Studi Internasional', 'DInt', 1),
('HCGD', 'Grup Pengembangan SDM', 'DSDM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_dokumen`
--

CREATE TABLE IF NOT EXISTS `jenis_dokumen` (
  `kode_jenisdok` varchar(10) NOT NULL,
  `jenis_dokumen` varchar(25) NOT NULL,
  PRIMARY KEY (`kode_jenisdok`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_dokumen`
--

INSERT INTO `jenis_dokumen` (`kode_jenisdok`, `jenis_dokumen`) VALUES
('BA', 'Berita Acara'),
('Faks', 'Faksimili'),
('M.01', 'Memorandum Korespondensi'),
('M.02', 'Memorandum Satuan Kerja'),
('NK', 'Nota Kesepahaman'),
('P', 'Perjanjian'),
('Peng', 'Pengumuman'),
('Rsl', 'Risalah'),
('Srt', 'Surat'),
('Srt.K', 'Surat Kuasa'),
('T', 'Telaahan');

-- --------------------------------------------------------

--
-- Table structure for table `lembar_disposisi`
--

CREATE TABLE IF NOT EXISTS `lembar_disposisi` (
  `nomor_ldp` int(6) NOT NULL,
  `kode_ddm` varchar(8) NOT NULL,
  `jenis_lembar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lembar_disposisi`
--

INSERT INTO `lembar_disposisi` (`nomor_ldp`, `kode_ddm`, `jenis_lembar`) VALUES
(1, '2', 'internal'),
(2, '3', 'internal'),
(3, '4', 'internal'),
(4, '9', 'internal');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `nip` varchar(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `nama_pegawai` varchar(40) NOT NULL,
  `password` varchar(100) NOT NULL,
  `jabatan` varchar(20) NOT NULL,
  `kode_divisi` varchar(15) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `thumb` varchar(60) NOT NULL,
  `aktif` int(1) NOT NULL,
  PRIMARY KEY (`nip`),
  UNIQUE KEY `nip` (`nip`),
  KEY `kode_divisi` (`kode_divisi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`nip`, `username`, `nama_pegawai`, `password`, `jabatan`, `kode_divisi`, `foto`, `thumb`, `aktif`) VALUES
('00001', 'admin', 'hendra prasetio', '21232f297a57a5a743894a0e4a801fc3', 'administrator', '', 'logo.png', 'logo_thumb.png', 1),
('08597', 'maryati', 'MARYATI HUDIYONO', 'a42320d4321a49ca902fdd16b6cf258f', 'pegawai', 'MPSI', 'Logo_Bank_BI.jpg', 'Logo_Bank_BI_thumb.jpg', 1),
('08601', 'tuti', 'TUTI H. LINGGAWASTU', '7da0da6bf56eb7dc3f1b10684b7c806e', 'pegawai', 'MPSI', 'Logo_Bank_BI1.jpg', 'Logo_Bank_BI1_thumb.jpg', 1),
('08727', 'lestiwati', 'LESTIYAWATI A.M.S', '4b8b2420289f971fd198cd9801903ef3', 'pegawai', 'POSI', '', '_thumb', 1),
('08773', 'lily', 'LILY INDRA H.', '89f288757f4d0693c99b007855fc075e', 'pegawai', 'PPA-PB', 'Logo_Bank_BI62.jpg', 'Logo_Bank_BI62_thumb.jpg', 1),
('09409', 'muhammad_amri', 'MUHAMMAD AMRI', 'ceed8986b6296f1fa0bee43b9cf69be7', 'pegawai', 'PASI', 'Logo_Bank_BI94.jpg', 'Logo_Bank_BI94_thumb.jpg', 1),
('09982', 'rini_harjanto', 'RINI HARJANTO', 'd546f94cde5855d53f62b0a7b099a8d0', 'pegawai', 'MPSI', 'Logo_Bank_BI10.jpg', 'Logo_Bank_BI10_thumb.jpg', 1),
('10085', 'yudi_hidayat', 'YUDI HIDAYAT ABIYUDA', '968227862acec3d16c008add7e6c8a70', 'pegawai', 'PSSI', '', '_thumb', 1),
('10101010', 'bismillah', 'bismillah', 'e172dd95f4feb21412a692e73929961e', 'pegawai', 'MPSI', 'dalu_kirom2.jpg', 'dalu_kirom2_thumb.jpg', 0),
('10570', 'yunus', 'YUNUS EMPI', 'a2c9a5d635f96695f809ce5272736ec5', 'pegawai', 'PASI', 'Logo_Bank_BI95.jpg', 'Logo_Bank_BI95_thumb.jpg', 1),
('10645', 'suraji', 'SURAJI', '79699ab180604b444d7a963ca2105540', 'pegawai', 'MPSI', 'Logo_Bank_BI3.jpg', 'Logo_Bank_BI3_thumb.jpg', 1),
('10748', 'niza', 'NIZA LELIANY', '874dd7b8887509ae09b31ba4adb7a70e', 'pegawai', 'MPSI', 'Logo_Bank_BI4.jpg', 'Logo_Bank_BI4_thumb.jpg', 1),
('10749', 'atik', 'ATIK W. EKA P.', '954d3c0cafce6df613bef3275ad1c7ff', 'pegawai', 'POSI', '', '_thumb', 1),
('10750', 'djonan', 'DJONAN ADJI G.T', 'bd9d85e49b3253992759ea56f13d18dd', 'pegawai', 'PASI', 'Logo_Bank_BI96.jpg', 'Logo_Bank_BI96_thumb.jpg', 1),
('11084', 'ari_sudardi', 'ARI RUKMINI SUDARDI', '4a8095107dcc54bbc5d97af01f6c2e70', 'pegawai', 'PSSI', '', '_thumb', 1),
('11280', 'agustinus', 'AGUSTINUS ARGODIANTORO', '39315ca51a0e725b4103063cbd90ec46', 'pegawai', 'PASI', 'Logo_Bank_BI97.jpg', 'Logo_Bank_BI97_thumb.jpg', 1),
('11319', 'sri_wijayanti', 'SRI WIJAYANTI', 'cd002e0a01893dcf835aaa741349de67', 'pegawai', 'PSSI', '', '_thumb', 1),
('11337', 'agustani', 'AGUSTANI', '2748ffa919a3d0f0f1141764db3a03d2', 'pegawai', 'POSI', '', '_thumb', 1),
('11365', 'lisa', 'LISA BINTI HS.', 'ed14f4a4d7ecddb6dae8e54900300b1e', 'pegawai', 'PASI', 'Logo_Bank_BI98.jpg', 'Logo_Bank_BI98_thumb.jpg', 1),
('11410', 'aris', 'ARIS WIDODO', '288077f055be4fadc3804a69422dd4f8', 'pegawai', 'PASI', 'Logo_Bank_BI99.jpg', 'Logo_Bank_BI99_thumb.jpg', 1),
('11413', 'dani', 'DANI HERACHMADI', '55b7e8b895d047537e672250dd781555', 'pegawai', 'POSI', '', '_thumb', 1),
('11423', 'surantino', 'SURANTINO', 'ecab02f65c05d29ff5891f704248995e', 'pegawai', 'PSSI', '', '_thumb', 1),
('11436', 'rosnelly', 'ROSNELLY D.P', 'ab1f321449e7058a5afb9df91e0c3ea0', 'pegawai', 'PSSI', '', '_thumb', 1),
('11444', 'suhardini', 'SUHARDINI ERNAWATI', 'cad0909a840b01f3ba6da7eee77f0ade', 'pegawai', 'POSI', '', '_thumb', 1),
('11537', 'djaka', 'DJAKA TRI MULJANA', '088a1bfd57dd0b341e4289ee45b5168a', 'pegawai', 'POSI', '', '_thumb', 1),
('11544', 'agoes', 'AGOES WEDI', '1feadc10e93f2b64c65868132f1e72d3', 'pegawai', 'POSI', 'dalu_kirom1.jpg', 'dalu_kirom1_thumb.jpg', 1),
('11701', 'sri_haryati', 'SRI HARYATI', '01946c90b33d514d8f4e478508ef3cd5', 'pegawai', 'PASI', '', '_thumb', 1),
('11711', 'bahari', 'BAHARI PRIYATNA', 'c62d618ed529bce8f5a52452cecda96c', 'pegawai', 'MPSI', 'Logo_Bank_BI5.jpg', 'Logo_Bank_BI5_thumb.jpg', 1),
('11784', 'marta', 'MARTA D. SUSETYO', 'a763a66f984948ca463b081bf0f0e6d0', 'pegawai', 'MPSI', 'Logo_Bank_BI6.jpg', 'Logo_Bank_BI6_thumb.jpg', 1),
('11792', 'nara', 'NARA HIYA', 'bb8b20c99f94d079cbd72677168255b7', 'pegawai', 'PPA-MO', 'Logo_Bank_BI47.jpg', 'Logo_Bank_BI47_thumb.jpg', 1),
('11813', 'deni', 'R. DENI MULYOSADHONO', '43f41d127a81c54d4c8f5f93daeb7118', 'pegawai', 'MPSI', 'Logo_Bank_BI7.jpg', 'Logo_Bank_BI7_thumb.jpg', 1),
('11885', 'titu', 'TITU PERMANA', 'a257a91001c2575f1989948a5c923e3c', 'pegawai', 'POSI', '', '_thumb', 1),
('12032', 'slamet', 'SLAMET HARIJADI', 'c5a42d9667c760e1b7c064e25536e570', 'pegawai', 'POSI', '', '_thumb', 1),
('12293', 'muhammad_achmad', 'MUHAMMAD ACHMAD', '35f57350b80e895f2a38e768a048891a', 'pegawai', 'PPA_MI', 'Logo_Bank_BI51.jpg', 'Logo_Bank_BI51_thumb.jpg', 1),
('12294', 'sandra', 'SANDRA PRANATA', 'f40a37048732da05928c3d374549c832', 'pegawai', 'PKSI', 'Logo_Bank_BI19.jpg', 'Logo_Bank_BI19_thumb.jpg', 1),
('12297', 'diah', 'DIAH ROSDIANA', 'b1980b34d5180cf2051d0fe400cb86e0', 'pegawai', 'PPA-SP', 'Logo_Bank_BI73.jpg', 'Logo_Bank_BI73_thumb.jpg', 1),
('12345', 'umar', 'Umar', '92deb3f274aaee236194c05729bfa443', 'pegawai', 'MPSI', '6a00e550199efb8833010536a5483e970c-800wi.jpg', '6a00e550199efb8833010536a5483e970c-800wi_thumb.jpg', 1),
('12404', 'robert', 'F.X. ROBERT GINTING', '684c851af59965b680086b7b4896ff98', 'pegawai', 'SKMI', 'Logo_Bank_BI29.jpg', 'Logo_Bank_BI29_thumb.jpg', 1),
('12567', 'tjatur', 'TJATUR FADJAR M.', '8887cb1044ad8f2329a525d1d6ef75eb', 'pegawai', 'MPSI', 'Logo_Bank_BI8.jpg', 'Logo_Bank_BI8_thumb.jpg', 1),
('12622', 'amir', 'T. AMIR HAMZAH', '63eefbd45d89e8c91f24b609f7539942', 'pegawai', 'PPA-PB', 'Logo_Bank_BI63.jpg', 'Logo_Bank_BI63_thumb.jpg', 1),
('12630', 'yulistian', 'YULISTIAN PANCAWIJAYA', 'eb226bcb5356ada7d287d794dd4735ce', 'pegawai', 'PPA-LS', 'Logo_Bank_BI85.jpg', 'Logo_Bank_BI85_thumb.jpg', 1),
('12638', 'djoko', 'DJOKO SUBIJONO', 'a60c6a387f984cacb74fd2c50bcdff56', 'pegawai', 'PPA-MO', 'Logo_Bank_BI48.jpg', 'Logo_Bank_BI48_thumb.jpg', 1),
('12683', 'linda', 'LINDA NURHAIDA', 'eaf450085c15c3b880c66d0b78f2c041', 'pegawai', 'PPA-PB', 'Logo_Bank_BI74.jpg', 'Logo_Bank_BI74_thumb.jpg', 1),
('12710', 'ratnawan', 'RATNAWAN BIMANTORO', 'b74551a425c345069e4dec0daa5c67de', 'pegawai', 'PPA-LS', 'Logo_Bank_BI86.jpg', 'Logo_Bank_BI86_thumb.jpg', 1),
('12714', 'fachriza', 'FACHRIZA', '59993add0a93a196d1a93acd4f699f99', 'pegawai', 'PKSI', 'Logo_Bank_BI20.jpg', 'Logo_Bank_BI20_thumb.jpg', 1),
('12723', 'djarot', 'DJAROT SUMANTRI', '2080faf109b33742207848327e8334a3', 'pegawai', 'POSI', '', '_thumb', 1),
('12736', 'prihantono', 'PRIHANTONO', 'cd94f5dea01771b532d79168c681e8ff', 'pegawai', 'STSI', '', '_thumb', 1),
('12780', 'sigit', 'SIGIT MARYUWANTO', '223a0fa8f15933d622b3c7a13f186027', 'pegawai', 'PSSI', '', '_thumb', 1),
('12873', 'benny', 'BENNY SADWIKO', '42f4b247702c99bda0fc7bcc41c70d19', 'pegawai', 'SKTI', '', '_thumb', 1),
('12917', 'wirasmoyo', 'WIRASMOYO', '6cbf1e358f54453e188b0da1a720c6f9', 'pegawai', 'PSSI', '', '_thumb', 1),
('12921', 'itjut', 'ITJUT NOERHAJATI', 'cdca208b55b540026c1810c2a48687d4', 'pegawai', 'MPSI', 'Logo_Bank_BI9.jpg', 'Logo_Bank_BI9_thumb.jpg', 1),
('13031', 'istianto', 'ISTIANTO UTOMO', '5567d0862433792714202eb7b87b49ad', 'pegawai', 'POSI', '', '_thumb', 1),
('13032', 'fajar', 'FAJAR EKA M.', '24bc50d85ad8fa9cda686145cf1f8aca', 'pegawai', 'PKSI', 'Logo_Bank_BI21.jpg', 'Logo_Bank_BI21_thumb.jpg', 1),
('1311414', 'secret', 'Mrs Secretary', '5ebe2294ecd0e0f08eab7690d2a6ee69', 'sekretaris', 'MPSI', 'chrissy_headshot_small1.jpg', 'chrissy_headshot_small1_thumb.jpg', 1),
('13134', 'aryanta', 'ARYANTA WIBAWA', 'afece5332689138f93b06ccf4abee215', 'pegawai', 'PPA-LS', 'Logo_Bank_BI87.jpg', 'Logo_Bank_BI87_thumb.jpg', 1),
('13153', 'budi', 'BUDI ADRIANTO', '00dfc53ee86af02e742515cdcf075ed3', 'pegawai', 'PKSI', 'Logo_Bank_BI22.jpg', 'Logo_Bank_BI22_thumb.jpg', 1),
('13159', 'freddy', 'FREDDY FADJARI', 'eda56def9e82a3936a75aff3f4e66330', 'pegawai', 'PPA-PB', 'Logo_Bank_BI64.jpg', 'Logo_Bank_BI64_thumb.jpg', 1),
('13471', 'adhitya', 'ADHITYA PRIYANKA', '8515a4039f88efec072208ba8c859e10', 'pegawai', 'PASI', '', '_thumb', 1),
('13473', 'ahmad_yandriyansyah', 'AHMAD YANDRIANSYAH', '1174162f85a9b50a7fa5c271de4fe40f', 'pegawai', 'STSI', '', '_thumb', 1),
('13490', 'burman', 'BURMAN NOVIANSYAH', '45018c4150614a97446ebf05d86435b9', 'pegawai', 'SKTI', '', '_thumb', 1),
('13500', 'diki', 'DIKI TEDRIANA', '43b93443937ea642a9a43e77fd5d8f77', 'pegawai', 'PPA-PB', 'Logo_Bank_BI75.jpg', 'Logo_Bank_BI75_thumb.jpg', 1),
('13539', 'jimmi', 'JIMMI ARZA HAMZAH', '67eddc24795f4b2c500a7dad119fbfab', 'pegawai', 'PPA-SP', 'Logo_Bank_BI76.jpg', 'Logo_Bank_BI76_thumb.jpg', 1),
('13551', 'linda_yunita', 'LINDA YUNITA FATIMAH', '25e2eb635347566f358961500685ba2a', 'pegawai', 'SKTI', '', '_thumb', 1),
('13569', 'mutiara', 'MUTIARA  PATRIA', 'f58e35e214c208599694b9d9b3b43a51', 'pegawai', 'PPA_MI', 'Logo_Bank_BI37.jpg', 'Logo_Bank_BI37_thumb.jpg', 1),
('13578', 'oktavianus', 'OKTAVIANUS SINAGA', 'a3b79ecd379e7f38805cb3dbd2baeb2a', 'pegawai', 'PPA-PB', 'Logo_Bank_BI65.jpg', 'Logo_Bank_BI65_thumb.jpg', 1),
('13585', 'rahardjono', 'RAHARDJONO', '6d28fc1a5f9b9be1037f67899b210a4b', 'pegawai', 'PPA_MI', 'Logo_Bank_BI38.jpg', 'Logo_Bank_BI38_thumb.jpg', 1),
('13646', 'agung', 'AGUNG TRISNANINGSIH', 'e59cd3ce33a68f536c19fedb82a7936f', 'pegawai', 'PKSI', 'Logo_Bank_BI23.jpg', 'Logo_Bank_BI23_thumb.jpg', 1),
('13651', 'boyke', 'BOYKE SIMANJUNTAK', '1e99e48ce096dd7ba8ae5318526cfa99', 'pegawai', 'POSI', '', '_thumb', 1),
('13655', 'eko', 'EKO RACHMANTO', 'e5ea9b6d71086dfef3a15f726abcc5bf', 'pegawai', 'SKTI', '', '_thumb', 1),
('13676', 'marice', 'MARICE TAMPUBOLON', '534f13b102fd95bcca6d187a0ad4bdc1', 'pegawai', 'PSSI', '', '_thumb', 1),
('13690', 'nabil', 'NABIL MUHAMMAD', '070aa66550916626673f492bdbdb655f', 'pegawai', 'POSI', '', '_thumb', 1),
('13697', 'nia', 'NIA RAHMAWATI', '04a481486dd84d7c8bfdfc89d38136a6', 'pegawai', 'PASI', '', '_thumb', 1),
('13704', 'nuke', 'NUKE NURLINAWATI P.', '6c2bfbd89d74ef47595fe6328a142427', 'pegawai', 'PASI', '', '_thumb', 1),
('13731', 'rini_arisanti', 'RINI ARISANTI', '6dbb5a136ef5d8f925c92f69d028adc9', 'pegawai', 'MPSI', 'Logo_Bank_BI11.jpg', 'Logo_Bank_BI11_thumb.jpg', 1),
('13745', 'samuel', 'SAMUEL ARBIAN', 'd8ae5776067290c4712fa454006c8ec6', 'pegawai', 'POSI', '', '_thumb', 1),
('13747', 'santi', 'SANTI MUTIA', 'ae1d4b431ead52e5ee1788010e8ec110', 'pegawai', 'POSI', '', '_thumb', 1),
('13764', 'syarief_hidayatullah', 'SYARIEF HIDAYATULLAH H.', 'b7855dadb4ba201ea0f8d9ddcc9dd5bb', 'pegawai', 'PSSI', '', '_thumb', 1),
('13768', 'titik', 'TITIK RAHAYU', '0503159ed6c0047367ec9c46da4c870d', 'pegawai', 'PPA-MO', 'Logo_Bank_BI49.jpg', 'Logo_Bank_BI49_thumb.jpg', 1),
('13776', 'virna', 'VIRNA PUSPITASARI', '3ea637f409210f00c303152d75c26d71', 'pegawai', 'PSSI', '', '_thumb', 1),
('13781', 'wahyu', 'WAHYU RIADI', '32c9e71e866ecdbc93e497482aa6779f', 'pegawai', 'PPA_MI', 'Logo_Bank_BI39.jpg', 'Logo_Bank_BI39_thumb.jpg', 1),
('14091', 'andre', 'ANDRE LISTYO WIBOWO', '19984dcaea13176bbb694f62ba6b5b35', 'pegawai', 'SKTI', '', '_thumb', 1),
('14092', 'syarief', 'SYARIEF', '858227e6c04bbadca0f5d44827aab219', 'pegawai', 'MPSI', 'Logo_Bank_BI12.jpg', 'Logo_Bank_BI12_thumb.jpg', 1),
('14094', 'rahardiyan', 'RAHARDIYAN', 'b7fe35b4e1d5647cb18b50ebba778c7e', 'pegawai', 'POSI', '', '_thumb', 1),
('14098', 'muhammad_alfan', 'MUHAMMAD ALFAN DHARMA', '0d3872a2255f0cd6c3d4c1a74254cea2', 'pegawai', 'PPA-MO', 'Logo_Bank_BI50.jpg', 'Logo_Bank_BI50_thumb.jpg', 1),
('14229', 'indra', 'INDRA ADHI WIBOWO', 'e24f6e3ce19ee0728ff1c443e4ff488d', 'pegawai', 'MPSI', 'Logo_Bank_BI13.jpg', 'Logo_Bank_BI13_thumb.jpg', 1),
('14253', 'piki', 'PIKI PAHLISA', 'c8ae906ff516b6287895602ed8de9991', 'pegawai', 'PPA-SP', 'Logo_Bank_BI77.jpg', 'Logo_Bank_BI77_thumb.jpg', 1),
('14275', 'sofia', 'SOFIA RITA', '17da1ae431f965d839ec8eb93087fb2b', 'pegawai', 'PPA_MI', 'Logo_Bank_BI40.jpg', 'Logo_Bank_BI40_thumb.jpg', 1),
('14366', 'marhendra', 'MARHENDRA LIDIANSA', '3670b95531e6d76105540cdd63c00469', 'pegawai', 'SKTI', '', '_thumb', 1),
('14381', 'mustofa', 'MUSTOFA KAMAL', 'e0449718f922b3ab6be915681a17fca8', 'pegawai', 'STSI', '', '_thumb', 1),
('14421', 'heru', 'HERU SUMANTO', 'a648ab9a3e32c5f3f6e9ddbd41c0530f', 'pegawai', 'PPA-SP', 'Logo_Bank_BI78.jpg', 'Logo_Bank_BI78_thumb.jpg', 1),
('14423', 'juliawaty', 'JULIAWATY I.', '17f99dea832dbdb806b91e1c677aeeb8', 'pegawai', 'PPA-LS', 'Logo_Bank_BI88.jpg', 'Logo_Bank_BI88_thumb.jpg', 1),
('14424', 'yudi', 'YUDI MULIAWIRAWAN SUGALIH', 'c232864d5de2064450915c0b9e4cc0b5', 'pegawai', 'PPA-MO', 'Logo_Bank_BI52.jpg', 'Logo_Bank_BI52_thumb.jpg', 1),
('14426', 'regi', 'REGI WIBOWO HUDYANA', 'a0e09290ac172c055b4e6fd0df46f0a5', 'pegawai', 'SKMI', 'Logo_Bank_BI31.jpg', 'Logo_Bank_BI31_thumb.jpg', 1),
('14427', 'reza', 'REZA REFIANTO', 'bb98b1d0b523d5e783f931550d7702b6', 'pegawai', 'PPA-MO', 'Logo_Bank_BI53.jpg', 'Logo_Bank_BI53_thumb.jpg', 1),
('14437', 'anastasia', 'ANASTASIA ALAMSYAH', 'fa17f85c91125ebe136de0a5fdd47951', 'pegawai', 'PPA_MI', 'Logo_Bank_BI41.jpg', 'Logo_Bank_BI41_thumb.jpg', 1),
('14444', 'candra', 'CANDRA TRI SURGO', '2614ae3c375c3095dc536283672548bd', 'pegawai', 'MPSI', 'Logo_Bank_BI14.jpg', 'Logo_Bank_BI14_thumb.jpg', 1),
('14452', 'dian', 'DIAN INDRIANI', 'f97de4a9986d216a6e0fea62b0450da9', 'pegawai', 'PKSI', 'Logo_Bank_BI24.jpg', 'Logo_Bank_BI24_thumb.jpg', 1),
('14455', 'dina', 'DINA MARIANA', 'e274648aed611371cf5c30a30bbe1d65', 'pegawai', 'PPA_MI', 'Logo_Bank_BI42.jpg', 'Logo_Bank_BI42_thumb.jpg', 1),
('14463', 'garuh', 'GARUH WALUYO EDY', '0ee192fa144e3913c789d1599c72749c', 'pegawai', 'POSI', '', '_thumb', 1),
('14478', 'kusnaidi', 'KUSNAIDI KARTA KESUMAH', '5a0669a87de3334ba03ab1500668ef3a', 'pegawai', 'PASI', '', '_thumb', 1),
('14485', 'mizwar', 'MIZWAR HAERUN ARIF', 'ca535385125d2e6010c532599626cb11', 'pegawai', 'STSI', '', '_thumb', 1),
('14487', 'muhammad_ridwan', 'MUHAMMAD RIDWAN', 'f4d1749fb29c6a37086e5ee0caefb97c', 'pegawai', 'PASI', '', '_thumb', 1),
('14518', 'syarifah', 'SYARIFAH NURELIZA', '3e32bbb8b5ccf141d67959af97904bc4', 'pegawai', 'PSSI', '', '_thumb', 1),
('14519', 'takkas', 'TAKKAS MARTUA H.M', '47562bb4e39ff1ef195a73b3af3ef347', 'pegawai', 'POSI', '', '_thumb', 1),
('14527', 'wahyu_dwi', 'WAHYU DWI PURTONO', '4bfb218bb76d6f0364d855bed98b96e0', 'pegawai', 'PPA-PB', 'Logo_Bank_BI66.jpg', 'Logo_Bank_BI66_thumb.jpg', 1),
('14531', 'yulia', 'YULIA ULFAH S.', '03be66295cd7eb6cf6001c9181bb904d', 'pegawai', 'PSSI', '', '_thumb', 1),
('14570', 'nohan', 'NOHAN BHADRIKA RATNA WIDYASTUT', '6e52850929569c10286ba89e15fac0b9', 'pegawai', 'PPA-PB', 'Logo_Bank_BI71.jpg', 'Logo_Bank_BI71_thumb.jpg', 1),
('14593', 'laksmi', 'LAKSMI EKA PUSPITASARI', '55e97a514130df1539106127e1eb41d5', 'pegawai', 'PPA-LS', 'Logo_Bank_BI89.jpg', 'Logo_Bank_BI89_thumb.jpg', 1),
('14670', 'iin', 'IIN MARLINA', 'f6a2e5ede47e66c7212ffaa258b7f5c8', 'pegawai', 'SKMI', 'Logo_Bank_BI32.jpg', 'Logo_Bank_BI32_thumb.jpg', 1),
('14671', 'setyo', 'SETYO KUNCORO', '6309647631df09e6caee8ad7c1704013', 'pegawai', 'PPA_MI', 'Logo_Bank_BI43.jpg', 'Logo_Bank_BI43_thumb.jpg', 1),
('14674', 'andry', 'ANDRY PRIMA SEMBIRING M.', '1fd07199cca4ff81d01dca373c6e03a9', 'pegawai', 'PPA-PB', 'Logo_Bank_BI67.jpg', 'Logo_Bank_BI67_thumb.jpg', 1),
('14939', 'yohana', 'YOHANA FRANSISCA WIDA IKASARI', 'd1ff1c32fb0a45cdc418abe85908aba2', 'pegawai', 'PPA-MO', 'Logo_Bank_BI55.jpg', 'Logo_Bank_BI55_thumb.jpg', 1),
('14951', 'ahmad', 'AHMAD MAIMUNIF', '61243c7b9a4022cb3f8dc3106767ed12', 'pegawai', 'PPA-MO', 'Logo_Bank_BI56.jpg', 'Logo_Bank_BI56_thumb.jpg', 1),
('14952', 'anita', 'ANITA SUKMA OKTAVIA SARAGIH', '83349cbdac695f3943635a4fd1aaa7d0', 'pegawai', 'PASI', '', '_thumb', 1),
('14956', 'faradila', 'FARADILA ANGGUN SURYANINGSIH', 'b74170a0d08278304b9ec275cf6fcd56', 'pegawai', 'PPA-PB', 'Logo_Bank_BI68.jpg', 'Logo_Bank_BI68_thumb.jpg', 1),
('14957', 'farid', 'FARID HAMDI', 'a1d12da42d4302e53d510954344ad164', 'pegawai', 'PPA-SP', 'Logo_Bank_BI79.jpg', 'Logo_Bank_BI79_thumb.jpg', 1),
('14960', 'ridwan', 'RIDWAN', 'd584c96e6c1ba3ca448426f66e552e8e', 'pegawai', 'MPSI', 'Logo_Bank_BI15.jpg', 'Logo_Bank_BI15_thumb.jpg', 1),
('14961', 'toni', 'TONI HARYANTO', 'aefe34008e63f1eb205dc4c4b8322253', 'pegawai', 'PKSI', 'Logo_Bank_BI25.jpg', 'Logo_Bank_BI25_thumb.jpg', 1),
('14964', 'indra_priyadi', 'INDRA PRIYADI', '8989b60eeaffecce473647f0611e6bc5', 'pegawai', 'POSI', '', '_thumb', 1),
('14965', 'rino', 'RINO YUDHA PRAKASA', '5056b3f5f952722139abe044ba1d25e5', 'pegawai', 'POSI', '', '_thumb', 1),
('14966', 'rizky', 'RIZKY ERIKO', '49d8712dd6ac9c3745d53cd4be48284c', 'pegawai', 'POSI', '', '_thumb', 1),
('14967', 'yunanto', 'YUNANTO AJI NUGROHO', '3c2ab224c6d501b2d59cbb9a03423f50', 'pegawai', 'POSI', '', '_thumb', 1),
('14978', 'desi', 'DESI HADIATI', '069e2dd171f61ecffb845190a7adf425', 'pegawai', 'POSI', '', '_thumb', 1),
('15017', 'weno', 'WENO ADJI SYAHDANA', '4d9706f56dfcca58815b32f2b3239d7a', 'pegawai', 'PPA-SP', 'Logo_Bank_BI80.jpg', 'Logo_Bank_BI80_thumb.jpg', 1),
('15131', 'aditia', 'ADITIA HERMAWAN', '55c828b40067e55ef2e146dfb95eb7ce', 'pegawai', 'PPA-SP', 'Logo_Bank_BI81.jpg', 'Logo_Bank_BI81_thumb.jpg', 1),
('15174', 'cakra', 'CAKRA WIRABUANA', '2a7d24a81b94a7d9d998d25994128c93', 'pegawai', 'PSSI', '', '_thumb', 1),
('15270', 'indra_purnama', 'INDRA PURNAMA', '5907c0b32b9c0df101371e771f536cc5', 'pegawai', 'PPA-LS', 'Logo_Bank_BI90.jpg', 'Logo_Bank_BI90_thumb.jpg', 1),
('15348', 'rachmat', 'RACHMAT', '2dbc7dd7e9524ddff1157d2e3df10aeb', 'pegawai', 'POSI', '', '_thumb', 1),
('15398', 'sri', 'SRI RAHAYU', 'd1565ebd8247bbb01472f80e24ad29b6', 'pegawai', 'PPA-PB', 'Logo_Bank_BI69.jpg', 'Logo_Bank_BI69_thumb.jpg', 1),
('15430', 'yeti', 'YETI SETIAWATI', '4e563b71ca562d5731ad94a208fa0af5', 'pegawai', 'PPA-LS', 'Logo_Bank_BI91.jpg', 'Logo_Bank_BI91_thumb.jpg', 1),
('15447', 'thesa', 'THESA  PASKA UTAMA', '7230f21e07593cfa8f5913b8c52545c0', 'pegawai', 'SKTI', '', '_thumb', 1),
('15468', 'wily', 'WILY RAMADAN', '90ebf137648de5494a1f3a837db5aee6', 'pegawai', 'PPA-MO', 'Logo_Bank_BI57.jpg', 'Logo_Bank_BI57_thumb.jpg', 1),
('15469', 'welly', 'WELLY RAMANTIKA', 'a9127950d4bf7970f69794b6827cf1eb', 'pegawai', 'PPA-PB', 'Logo_Bank_BI70.jpg', 'Logo_Bank_BI70_thumb.jpg', 1),
('15471', 'marethania', 'MARETHANIA HALLEY PASALLI', 'ad63820386afe7801377417c11e88229', 'pegawai', 'PPA-SP', 'Logo_Bank_BI82.jpg', 'Logo_Bank_BI82_thumb.jpg', 1),
('15472', 'tata', 'TATA SUBRANTA', '49d02d55ad10973b7b9d0dc9eba7fdf0', 'pegawai', 'PPA-MO', 'Logo_Bank_BI58.jpg', 'Logo_Bank_BI58_thumb.jpg', 1),
('15473', 'ida', 'IDA BGS EDY DHARMA ENDRAYANA S', '7f78f270e3e1129faf118ed92fdf54db', 'pegawai', 'PPA-SP', 'Logo_Bank_BI83.jpg', 'Logo_Bank_BI83_thumb.jpg', 1),
('15487', 'laras', 'LARAS AYUTIRTA PRAMESTI', 'cc5a9fb4b1b93629e9aa413b8bf00fe9', 'pegawai', 'SKMI', 'Logo_Bank_BI33.jpg', 'Logo_Bank_BI33_thumb.jpg', 1),
('15497', 'panji', 'PANJI PUTRA SITORUS', 'd6b16b990a41b83f81a58d38ad7265f1', 'pegawai', 'PASI', '', '_thumb', 1),
('15504', 'kartika', 'KARTIKA CAROLINE', '2aca90f14de1638d56273cf4ff6b537d', 'pegawai', 'SKMI', 'Logo_Bank_BI34.jpg', 'Logo_Bank_BI34_thumb.jpg', 1),
('15515', 'krisna', 'KRISNA SETIOAJI', '948f5cc9f8c6c3b86a070beaca7d20bf', 'pegawai', 'PPA-MO', 'Logo_Bank_BI59.jpg', 'Logo_Bank_BI59_thumb.jpg', 1),
('15539', 'andara', 'ANDARA LIVIA', 'd41d8cd98f00b204e9800998ecf8427e', 'pegawai', 'PKSI', 'Logo_Bank_BI27.jpg', 'Logo_Bank_BI27_thumb.jpg', 1),
('15542', 'muhammad_juliandri', 'MUHAMMAD JULIANDRI', '64b77c97d9837652e865fc4894112d9a', 'pegawai', 'SKTI', '', '_thumb', 1),
('15545', 'ari', 'ARI ADITYA NUGRAHA', 'fc292bd7df071858c2d0f955545673c1', 'pegawai', 'MPSI', 'Logo_Bank_BI18.jpg', 'Logo_Bank_BI18_thumb.jpg', 1),
('15561', 'ias', 'IAS ARI MAHAPUTRA NAIBAHO', '77fe668dd6a6e411eb6c22fb9c1cfb0b', 'pegawai', 'PPA_MI', 'Logo_Bank_BI44.jpg', 'Logo_Bank_BI44_thumb.jpg', 1),
('15609', 'angga', 'ANGGA ARIANTO PRIBADI', '8479c86c7afcb56631104f5ce5d6de62', 'pegawai', 'SKTI', '', '_thumb', 1),
('15619', 'wahnan', 'WAHNAN AHMAL ASYSYAKIRY', '602292ade6dc7c5e2e684fc05cb3bdb0', 'pegawai', 'PPA-LS', 'Logo_Bank_BI92.jpg', 'Logo_Bank_BI92_thumb.jpg', 1),
('15891', 'frisca', 'FRISCA', 'c28197f833ad6c4fcdffb15db7f606a4', 'pegawai', 'STSI', '', '_thumb', 1),
('15909', 'nita', 'NITA ARRYANI SARI', '1eb6d605e0698d0c6d3121c8cd45e6b5', 'pegawai', 'PPA-MO', 'Logo_Bank_BI60.jpg', 'Logo_Bank_BI60_thumb.jpg', 1),
('15932', 'anshari', 'ANSHARI ARMY', 'b5e68f89ebcd9d85c7c7bec7fd09706f', 'pegawai', 'PKSI', 'Logo_Bank_BI28.jpg', 'Logo_Bank_BI28_thumb.jpg', 1),
('15934', 'doni', 'DONI SETYAWAN', '2da9cd653f63c010b6d6c5a5ad73fe32', 'pegawai', 'SKMI', 'Logo_Bank_BI35.jpg', 'Logo_Bank_BI35_thumb.jpg', 1),
('15968', 'ardi', 'ARDY TEDJAWIDJAJA', '0264391c340e4d3cbba430cee7836eaf', 'pegawai', 'MPSI', 'Logo_Bank_BI17.jpg', 'Logo_Bank_BI17_thumb.jpg', 1),
('15969', 'aressa', 'ARESSA WARDOYO', '65d5f7247dd43417fdda0b857df47893', 'pegawai', 'PASI', '', '_thumb', 1),
('16018', 'febryansyah', 'FEBRYANSYAH DJATU RACHMANAN', 'b51e076981864f2a2400c3127c610ba9', 'pegawai', 'POSI', '', '_thumb', 1),
('16106', 'ranny', 'RANNY SETYASTUTI', '26f1ae004d073eec0ba37a0d4f414fd2', 'pegawai', 'PSSI', '', '_thumb', 1),
('16148', 'andar', 'ANDAR SUROSO SITOHANG', '16cc89f2efb5015d0621456a113d8491', 'pegawai', 'SKTI', '', '_thumb', 1),
('16149', 'andi', 'ANDI ARI', 'ce0e5bf55e4f71749eade7a8b95c4e46', 'pegawai', 'SKTI', '', '_thumb', 1),
('16150', 'andri', 'ANDRI BAGUS NUGRAHA', '6bd3108684ccc9dfd40b126877f850b0', 'pegawai', 'PPA-PB', 'Logo_Bank_BI72.jpg', 'Logo_Bank_BI72_thumb.jpg', 1),
('16153', 'ditto', 'DITTO NARAPRATAMA', '13d9173fd8c031b139be36976e39614a', 'pegawai', 'PPA-MO', 'Logo_Bank_BI61.jpg', 'Logo_Bank_BI61_thumb.jpg', 1),
('16154', 'fajar_tricandra', 'FAJAR TRICANDRA PURNAMA', 'e2f6d9c136cd13f4c924c9f4351700af', 'pegawai', 'POSI', '', '_thumb', 1),
('16157', 'geri', 'GERI NOORZAMAN', '3360e4f799b5aba418a022b0373a876b', 'pegawai', 'PPA-SP', 'Logo_Bank_BI84.jpg', 'Logo_Bank_BI84_thumb.jpg', 1),
('16161', 'muhammad_yudha', 'MUHAMMAD YUDHA ADHIATMA', '061f9ac3b9d2d53e7513b0c0b1b8eac1', 'pegawai', 'PPA-LS', 'Logo_Bank_BI93.jpg', 'Logo_Bank_BI93_thumb.jpg', 1),
('16162', 'rendi', 'RENDI BUDIMAN ', 'd209fc47646bba5e5fdc3d3bbaad4b9c', 'pegawai', 'PPA_MI', 'Logo_Bank_BI45.jpg', 'Logo_Bank_BI45_thumb.jpg', 1),
('16163', 'rian', 'RIAN HADISAPUTRA', 'cb2b28afc2cc836b33eb7ed86f99e65a', 'pegawai', 'PPA_MI', 'Logo_Bank_BI46.jpg', 'Logo_Bank_BI46_thumb.jpg', 1),
('16164', 'vender', 'VENDER YANTO SALIM', '1a19b39699d9a03071c1478b3f425ea0', 'pegawai', 'POSI', '', '_thumb', 1),
('2111031', 'steve', 'Steve', 'd69403e2673e611d4cbd3fad6fd1788e', 'pegawai', 'PASI', 'steve.jpg', 'steve_thumb.jpg', 1),
('21716', 'herry', 'HERRY HERYANDA', '673491c1a59d25086ac3c58964dae71a', 'pegawai', 'SKMI', 'Logo_Bank_BI30.jpg', 'Logo_Bank_BI30_thumb.jpg', 1),
('8724', 'ucu', 'UCU S. SUKAMTO', '3bf642c0dcc3df2d2b99d97b1b712891', 'pegawai', 'PSSI', '', '_thumb', 1);

-- --------------------------------------------------------

--
-- Table structure for table `penerima_disposisi`
--

CREATE TABLE IF NOT EXISTS `penerima_disposisi` (
  `kode_penerima` int(11) NOT NULL,
  `kode_disposisi` varchar(10) NOT NULL,
  `nip` varchar(11) NOT NULL,
  `cek` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`kode_penerima`),
  KEY `kode_disposisi` (`kode_disposisi`),
  KEY `nip` (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penerima_disposisi`
--

INSERT INTO `penerima_disposisi` (`kode_penerima`, `kode_disposisi`, `nip`, `cek`) VALUES
(1, 'D_1_3', '11711', 0),
(2, 'D_2_3', '14444', 0),
(3, 'D_1_4', '12567', 1),
(4, 'D_2_4', '11711', 0),
(5, 'D_1_9', '12567', 1),
(6, 'D_2_9', '14444', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tahun_buku`
--

CREATE TABLE IF NOT EXISTS `tahun_buku` (
  `kode_tahun` int(2) NOT NULL,
  `tahun_buku` int(4) NOT NULL,
  `tahun_dokumen` int(4) NOT NULL,
  PRIMARY KEY (`kode_tahun`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun_buku`
--

INSERT INTO `tahun_buku` (`kode_tahun`, `tahun_buku`, `tahun_dokumen`) VALUES
(18, 2018, 2016),
(19, 2019, 2017);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ddk`
--
ALTER TABLE `ddk`
  ADD CONSTRAINT `ddk_ibfk_2` FOREIGN KEY (`kode_tahun`) REFERENCES `tahun_buku` (`kode_tahun`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ddk_ibfk_3` FOREIGN KEY (`kode_jenisdok`) REFERENCES `jenis_dokumen` (`kode_jenisdok`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ddm`
--
ALTER TABLE `ddm`
  ADD CONSTRAINT `ddm_ibfk_2` FOREIGN KEY (`kode_tahun`) REFERENCES `tahun_buku` (`kode_tahun`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ddm_ibfk_3` FOREIGN KEY (`kode_jenisdok`) REFERENCES `jenis_dokumen` (`kode_jenisdok`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `disposisi`
--
ALTER TABLE `disposisi`
  ADD CONSTRAINT `disposisi_ibfk_3` FOREIGN KEY (`kode_ddm`) REFERENCES `ddm` (`kode_ddm`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `disposisi_ibfk_5` FOREIGN KEY (`dari`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `divisi`
--
ALTER TABLE `divisi`
  ADD CONSTRAINT `divisi_ibfk_1` FOREIGN KEY (`kode_departemen`) REFERENCES `departemen` (`kode_departemen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `penerima_disposisi`
--
ALTER TABLE `penerima_disposisi`
  ADD CONSTRAINT `penerima_disposisi_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penerima_disposisi_ibfk_2` FOREIGN KEY (`kode_disposisi`) REFERENCES `disposisi` (`kode_disposisi`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
